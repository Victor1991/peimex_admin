import { abilitiesPlugin } from '@casl/vue'

import Vue from 'vue'
/*
COMPONENTES de cursos
*/
import FiltrosTablaCursos from './components/admin/cursos/FiltrosTablaCursos.vue'
import TablaCursos from './components/admin/cursos/TablaCursos.vue'
import FormularioCursos from './components/admin/cursos/FormularioCursos.vue'
import FormuarioPresentacion from './components/admin/cursos/FormuarioPresentacion.vue'
import FormularioClientesCursos from './components/admin/cursos/FormularioClientesCursos.vue'
// Modulos
import ListaModulos from './components/admin/cursos/ListaModulos.vue'
import FormularioModulo from './components/admin/cursos/FormularioModulo.vue'
// materiales
import FormularioMaterial from './components/admin/cursos/FormularioMaterial.vue'
// Post modulo
import FormularioPostModulo from './components/admin/cursos/FormularioPostModulo.vue'
import FormularioPreguntas from './components/admin/cursos/FormularioPreguntas.vue'
// Examenes
import Examen from './components/admin/cursos/Examen.vue'
import FormularioExamen from './components/admin/cursos/FormularioExamen.vue'
import PreguntasExamen from './components/admin/cursos/PreguntasExamen.vue'
import FormularioPreguntasExamen from './components/admin/cursos/FormularioPreguntasExamen.vue'

// Porefesores
import ListaProfesores from './components/admin/cursos/ListaProfesores.vue'
import FormularioProfesores from './components/admin/cursos/FormularioProfesores.vue'

// Relacionados
import ListaRelacionados from './components/admin/cursos/ListaRelacionados.vue'
import FormularioMaterialRelacionado from './components/admin/cursos/FormularioMaterialRelacionado.vue'

// Modal Upload Presentacion
import ModalUploadPresentacion from './components/generales/ModalUploadPresentacion.vue'


/*
COMPONENTES de USUARIOS
*/
import TablaUsuarios from './components/admin/usuarios/TablaUsuarios.vue'
import FiltrosTablaUsuarios from './components/admin/usuarios/FiltrosTablaUsuarios.vue'
import FormularioUsuario from './components/admin/usuarios/FormularioUsuario.vue'
import ModalInput from './components/generales/ModalInput.vue'


/*
COMPONENTES de ALUMNOS
*/
import TablaAlumnos from './components/admin/alumnos/TablaAlumnos.vue'
import FiltrosTablaAlumnos from './components/admin/alumnos/FiltrosTablaAlumnos.vue'
import FormularioAlumnos from './components/admin/alumnos/FormularioAlumnos.vue'
import FormularioCursosInscritos from './components/admin/alumnos/FormularioCursosInscritos.vue'


import ModalUpload from './components/generales/ModalUpload.vue'
import ModalUploadMaterial from './components/generales/ModalUploadMaterial.vue'
import ModalUploadProfesores from './components/generales/ModalUploadProfesores.vue'
import ModalUploadRelacionados from './components/generales/ModalUploadRelacionados.vue'
import ModalUploadSmall from './components/generales/ModalUploadSmall.vue'



import ability from './core/acl'
import BootstrapVue from 'bootstrap-vue'

import Vuex from 'vuex'
import Vueditor from 'vueditor'
import VueSweetalert2 from 'vue-sweetalert2';

const options = {
  confirmButtonColor: '#41b882',
  cancelButtonColor: '#ff7674'
}

Vue.use(VueSweetalert2, options)

import 'vueditor/dist/style/vueditor.min.css'

// your config here
let config ={
  toolbar: [
       'removeFormat', 'undo', 'redo', '|', 'element', 'fontName', 'fontSize', 'foreColor', 'backColor', 'divider', 'bold', 'italic', 'underline', 'strikeThrough',
         'link', 'unLink', 'divider', 'subscript', 'superscript', 'divider', 'justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull',
         '|', 'indent', 'outdent', 'insertOrderedList', 'insertUnorderedList', '|', 'emoji', 'table', '|', 'fullscreen', 'sourceCode'
  ],
  fontName: [
    {val: 'arial'},
    {val: 'arial narrow'},
    {val: 'arial rounded'},
    {val: 'arial hebrew'},
    {val: 'arial black'},
    {val: 'arial unicode ms'},
    {val: 'times new roman'},
    {val: 'Courier New'}
  ],
  fontSize: [
    '6px', '8px', '10px', '12px', '14px', '16px', '18px', '20px', '24px', '28px', '32px', '36px'
  ],
};

Vue.use(Vuex);
Vue.use(Vueditor, config);

// load
import Loading from 'vue-loading-overlay';
    // Import stylesheet
import 'vue-loading-overlay/dist/vue-loading.css';
    // Init plugin
Vue.use(Loading);

import CKEditor from 'ckeditor4-vue';
Vue.use( CKEditor );

Vue.use(abilitiesPlugin, ability, BootstrapVue);

window.Event = new Vue();
window.VueEvent = new Vue();

let vm = new Vue({
     el: '#app',
     components: {
          ModalUpload,
          ModalUploadPresentacion,
          ModalUploadMaterial,
          ModalUploadProfesores,
          ModalUploadRelacionados,
          ModalUploadSmall,
          /* USUARIOS */
          TablaUsuarios,
          FiltrosTablaUsuarios,
          FormularioUsuario,
          ModalInput,
          /* Cursos */
          TablaCursos,
          FiltrosTablaCursos,
          FormularioCursos,
          'formulario-clientes-cursos':FormularioClientesCursos,
          'formulario-cursos-inscritos':FormularioCursosInscritos,
          'form-presentacion': FormuarioPresentacion,
          // Mudulos
          ListaModulos,
          'fomulario-modulo':FormularioModulo,
          // Materiales
          'fomulario-materiales':FormularioMaterial,
          // PostModulo
          'fomulario-post-modulo':FormularioPostModulo,
          'fomulario-preguntas':FormularioPreguntas,
          // Examen
          Examen,
          PreguntasExamen,
          'fomulario-preguntas-examen':FormularioPreguntasExamen,
          'fomulario-examen':FormularioExamen,
          // Profesores
          'lista-profesores':ListaProfesores,
          'formulario-profesores':FormularioProfesores,
          // Relacionados
          'lista-relacionados':ListaRelacionados,
          'formulario-material-relacionados':FormularioMaterialRelacionado,
          /* Alumnos */
          TablaAlumnos,
          FiltrosTablaAlumnos,
          FormularioAlumnos
     }
});
