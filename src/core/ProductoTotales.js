export let producto_totales = {
    methods: {
        precio_neto_por_producto(producto){
            return Number(producto.precio_base) * Number(producto.a_surtir);
        },
        iva_total_por_producto: function (producto) {
            //return Number(producto.iva) ? this.precio_neto_por_producto(producto)  * .16 : 0;
            let iva = Number(producto.iva) ? this.precio_producto_despues_descuentos(producto) * .16 : 0;
            //console.log("iva", iva);
            return iva;
        },
        precio_neto_mas_iva_por_producto(producto){
            //return this.precio_neto_por_producto(producto) + this.iva_total_por_producto(producto)
            let neto = this.precio_producto_despues_descuentos(producto) + this.iva_total_por_producto(producto);
            //console.log("precio neto + iva", neto);
            return neto;
        },
        precio_producto_despues_descuentos(producto) {
            let precio = this.precio_neto_por_producto(producto) - this.monto_descuento_por_producto(producto);
            //console.log("precio dd", precio);
            return precio;
        },
        monto_descuento_por_producto(producto){
            //return this.precio_neto_mas_iva_por_producto(producto)  * Number(producto.descuento)/100;
            return this.precio_neto_por_producto(producto) * Number(producto.descuento) / 100;
        },
        precio_final_por_producto(producto){
            //return this.precio_neto_mas_iva_por_producto(producto) - this.monto_descuento_por_producto(producto);
            let precio_final = this.precio_neto_mas_iva_por_producto(producto);
            //console.log("precio final", precio_final);
            return precio_final;
        }
    },
};