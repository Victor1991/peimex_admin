export default Vue.extend({
    props: {
        data: {
            type: Array,
            required: true
        }
    },
    data(){
        return {
            items: this.data
        }
    },
    methods: {
        add(item){
            this.items.push(item);
        },
        update(item){
            let index = this.items.findIndex(x => x.id === item.id);
            if(index === -1){
                return  this.add(item);
            }else{
                this.$set(this.$data.items, index, item);
            }
        },
        remove(index){
            this.items.splice(index, 1);
        }
    }
});