import Errors from './Errors.js'


class FormHelper{

    constructor(){
        this.errores = new Errors();
        this.enviando = false;
    }

    notificacion(error, mensaje){
        if(!error){
            return toastr.success(mensaje, 'Éxito', {"closeButton": true});
        }
        return toastr.error(mensaje, 'Hubo un error', {"closeButton": true});
    }

    handler_errores(errores){
        if(errores){
            return this.errores.record(errores);
        }
        return this.errores.clear();

    }

    titulo_formulario(id){
        if(id){
            return 'Actualizar';
        }
        return 'Nuevo'
    }

    titulo_button(id){
        if(id){
            return 'Actualizar';
        }
        return 'Guardar'
    }


}

export default FormHelper
