class Alerts {

    warning (mensaje){
        $.uiAlert({
            textHead: 'Cuidado', // header
            text: mensaje, // Text
            bgcolor: '#F2711C', // background-color
            textcolor: '#fff', // color
            position: 'top-right',// position . top And bottom ||  left / center / right
            icon: 'warning sign', // icon in semantic-UI
            time: 2, // time
        });
    }

    error (mensaje){
        $.uiAlert({
            textHead: 'Error', // header
            text: mensaje, // Text
            bgcolor: '#DB2828', // background-color
            textcolor: '#fff', // color
            position: 'top-right',// position . top And bottom ||  left / center / right
            icon: 'warning sign', // icon in semantic-UI
            time: 3, // time
        });
    }

    success(mensaje){
        $.uiAlert({
            textHead: 'Éxito', // header
            text: mensaje, // Text
            bgcolor: '#19c3aa', // background-color
            textcolor: '#fff', // color
            position: 'top-right',// position . top And bottom ||  left / center / right
            icon: 'check', // icon in semantic-UI
            time: 3, // time
        });
    }

}
export default Alerts
