class Catalogos {

    constructor(){
        this.paises = [];
    }

    get_paises() {
        $.ajax({
            url: base_url + "catalogos/paises/",
            dataType: "json",
            success: function (response) {
                this.paises = response;
            }.bind(this),
            error: function (jqXHR, request) {
            }
        });
    }

}

export default Catalogos
