export let tabla = {
    data () {
        return {
            data: [],
            result_count: 0,
            is_sending: false,
        };
    },
    created(){
        Event.$on('is-seanding', () =>
            {
                this.$data.is_sending = true;
            }
        );
        Event.$on('finished-seanding', (respuesta) =>
            {
                this.$data.is_sending = false;
                this.$data.result_count = respuesta.count;
                this.$data.data = respuesta.data;
            }
        );
    }
};
