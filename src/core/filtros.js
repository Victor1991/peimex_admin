import Vue from 'vue'

Vue.filter('dinero', value => '$ '+Number(parseFloat(value).toFixed(2)).toLocaleString('en', {minimumFractionDigits: 2}));
Vue.filter('moneda', value => Number(parseFloat(value).toFixed(2)).toLocaleString('en', {minimumFractionDigits: 2}));
Vue.filter('numero', value => Number(value));

Vue.filter('estatus_producto', function (value) {
    switch(value) {
        case '3':
            return '<span class="tooltip-demo"><i class="fa fa-close text-danger" data-toggle="tooltip" data-placement="top" title="NO DISPONIBLE: Temporalmente agotado"></i></span>';
        case '1':
            return '<span class="tooltip-demo"><i class="fa fa-check" style="color: #1ab394" data-toggle="tooltip" data-placement="top" title="DISPONIBLE: Entrega de 1 a 2 días hábiles"></i></span>';
        case '2':
            return '<span class="tooltip-demo"><i class="fa fa-warning text-warning" data-toggle="tooltip" data-placement="top" title="LIMITADO: Pocas piezas en inventario o Corta Caducidad"></i></span>';
    }
});
