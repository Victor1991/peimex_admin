export let mixins = {
    data: function () {
        return { }
    },
    methods: {
        subtotal_por_producto: function (producto) {
            // this.$set(producto, 'subtotal_por_producto', (Number(producto.precio_base) * Number(producto.a_surtir)));
            return (Number(producto.precio_base) * Number(producto.a_surtir));
        },
        total_iva_por_producto: function (producto) {
            // this.$set(producto, 'total_iva_por_producto', ((Number(producto.iva)) * Number(producto.a_surtir)));
            return (
                ( (Number(producto.iva) * .16) * Number(producto.precio_base)) * Number(producto.a_surtir));
        },
        total_por_producto: function (producto) {
            // this.$set(producto, 'total_por_producto', ((Number(producto.precio_base) + Number(producto.iva)) * Number(producto.a_surtir)));
            return ((Number(producto.precio_base) + (Number(producto.iva) * .16) * Number(producto.precio_base)) * Number(producto.a_surtir))
        },
        cambios_en_producto: function (producto) {
            if(Number(producto.a_surtir) === 0){
                return '<span class="tooltip-demo"><i class="fa fa-exclamation-circle text-danger" data-toggle="tooltip" data-placement="top" title="No se pudo surtir este producto"></i></span>';
            }
            if(Number(producto.cantidad) > Number(producto.a_surtir)){
                return "<span class='tooltip-demo'><i class='fa fa-warning text-warning' data-toggle='tooltip' data-placement='top' title='Cambios en pedido: -"+(Number(producto.cantidad)-Number(producto.a_surtir))+" producto(s)'></i></span>";
            }
            if(Number(producto.cantidad) < Number(producto.a_surtir)){
                return '<span class="tooltip-demo"><i class="fa fa-check text-success" data-toggle="tooltip" data-placement="top" title="Se agregaron más productos"></i></span>';
            }
            return '';
        },
        row_class: function (producto) {
            if(Number(producto.a_surtir) === 0){
                return 'alert-danger';
            }
            if(Number(producto.cantidad) > Number(producto.a_surtir)){
                return 'alert-warning';
            }
            if(Number(producto.cantidad) < Number(producto.a_surtir)){
                return 'alert-success';
            }
            return false;
        },
    }
};