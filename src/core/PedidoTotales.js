import { producto_totales } from './ProductoTotales'

export let pedido_totales = {
    mixins: [ producto_totales ],
    computed: {
        no_productos: function () {
            return Object.values(this.productos).reduce(
                (previo, producto) => {
                    return Number(producto.a_surtir) > 0 ? previo + 1 : previo;
                }, 0
            );
        },
        subtotal: function () {
            return Object.values(this.productos).reduce(
                (previo, producto) => {
                    //return previo + this.precio_neto_por_producto(producto);
                    return previo + this.precio_producto_despues_descuentos(producto);
                }, 0
            );
        },
        iva: function () {
            let iva_productos = Object.values(this.productos).reduce(
                (previo, producto) => {
                    return previo + this.iva_total_por_producto(producto);
                }, 0
            );
            return iva_productos + (this.costo_envio * 0.16);
        },
        monto_descuento(){
            return Object.values(this.productos).reduce(
                (previo, producto) => {
                    return previo + this.monto_descuento_por_producto(producto);
                }, 0
            );
        },
        total: function () {
            let total = Object.values(this.productos).reduce(
                (prev, producto) => {
                    return prev + this.precio_final_por_producto(producto);
                }, 0
            );
            return total + (this.costo_envio * 1.16);
        }
    }
};