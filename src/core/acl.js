import { AbilityBuilder } from '@casl/ability'

function subjectName(item) {
    if (!item || typeof item === 'string') {
        return item
    }

    return item.__type
}

export default AbilityBuilder.define({ subjectName }, (can, cannot) => {
    // Object.keys(acl).forEach(function (permiso){
        // if(acl[permiso].valor){
        //     return can(acl[permiso].clave, acl[permiso].modulo)
        // }
        // return cannot(acl[permiso].clave, acl[permiso].modulo)
    // });
})
