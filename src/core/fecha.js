export let fecha = {
    data(){
        return{
            dias: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            meses: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
        }
    },
    methods: {
        parseaFecha(d){
            let date = new Date(d);
            return this.dias[date.getUTCDay()] + ' '+ date.getUTCDate() + ' de ' + this.meses[date.getUTCMonth()] + ' del ' + date.getUTCFullYear();
        }
    }
};