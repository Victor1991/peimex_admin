import _ from 'lodash';

export let tablaFiltros = {
    methods: {
        limpiar_filtros: function () {
            Object.keys(this.form).forEach(key => {
                if(key != 'range'){
                    this.form[key] = '';
                }
            });
        },
        enviar_filtros: _.debounce(function () {
            $.ajax({
                url: this.controller,
                data: this.$data.form,
                type: 'POST',
                dataType: 'json',
                beforeSend: function () {
                    Event.$emit('is-seanding');
                },
                success: function (respuesta) {
                    Event.$emit('finished-seanding', respuesta);
                }.bind(this)
            });
        }, 500)
    },
    watch: {
        form: {
            handler: function() {
                this.enviar_filtros();
            },
            deep: true
        }
    },
    created(){
        Event.$on('page-changed', ([pagina, total]) => {
               if (((pagina*total)-total) > 0 ) {
                    this.$data.form.range = [(pagina*total)-total, total];
               }else{
                    this.$data.form.range = [0, total];

               }
            }
        );
    }
};
