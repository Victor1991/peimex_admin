class Errors {

    constructor(){
        this.errores = {};
    }

    has(campo){
        return this.errores.hasOwnProperty(campo);
    }

    get(campo){
        if(this.errores[campo]){
            return this.errores[campo];
        }
    }

    set(campo, error){
        this.errores[campo] = error;
    }

    any(){
        return Object.keys(this.errores).length > 0;
    }

    record(errores){
        this.errores = errores;
    }

    clear(campo){
        if(campo){
            delete this.errores[campo];
            return true;
        }

        return this.errores = {};
    }

}

export default Errors
