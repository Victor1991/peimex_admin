<?php

/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/

class MY_Controller extends CI_Controller {
     public $modulo;
     public $nombre_modulo;
     public $rol_id_user;
     public function __construct() {

          parent::__construct();

          $this->load->helper(array('date', 'alert', 'validation_rules'));

          $this->load->library('mailhelper');


          $ci = get_instance();
          $ci->assets['js'] = array();
          $ci->assets['css'] = array();

     }

     public function add_asset($type, $var) {
          array_push(get_instance()->assets[$type], $var);
     }


}

class Admin_Controller extends MY_Controller {
     private $template;

     public function __construct() {

          parent::__construct();

          $this->load->model(array('usuario_model','administrador_model', 'rol_model', 'catalogo_model'));

          $this->load->library(array('auth', 'breadcrumbs'));


          if (!$this->auth->is_logged_in()) {
               redirect('login');
          }


          $this->usuario = $this->administrador_model->get_by_id($this->session->userdata('admin')['usuario_id']);

          $this->usuario->rol = $this->rol_model->get_by_id($this->usuario->rol_id);
          $this->load->library('acl');
          $this->breadcrumbs->push('<i class="fa fa-dashboard"></i> Inicio', 'admin');
          $this->rol_id_user = $this->session->userdata('admin')['rol_id'];

     }

     function view($view, $vars = array(), $string = false) {
          //if there is a template, use it.
          $template = '';
          if ($this->template) {
               $template = $this->template . '_';
          }

          if ($string) {
               $result = $this->load->view('admin/' . $template . 'header', $vars, true);
               $result .= $this->load->view('admin/' . $view, $vars, true);
               $result .= $this->load->view('admin/' . $template . 'footer', $vars, true);

               return $result;
          } else {
               $this->load->view('admin/' . $template . 'header', $vars);
               $this->load->view('admin/' . $view, $vars);
               $this->load->view('admin/' . $template . 'footer', $vars);
          }

          //reset $this->template to blank
          $this->template = false;
     }

     /* Template is a temporary prefix that lasts only for the next call to view */
     function set_template($template) {
          $this->template = $template;
     }

}

class Public_Controller extends MY_Controller {
     public function __construct() {

          parent::__construct();

          $this->load->model(array('catalogo_model'));



          /*
          if (!$this->auth->is_logged_inCliente()) {
          redirect('loginClientes');
     }
     */

     //load the theme package
     //        $this->load->add_package_path(APPPATH.'themes/default/');
     //        $this->load->helper(array('date', 'front', 'pagination', 'image', 'posts'));
     //        $this->load->model(array('categoria_model', 'post_model', 'menu_model'));

}

function view($view, $vars = array(), $string = false) {
     if ($string) {
          $result = $this->load->view('front/header', $vars, true);
          $result .= $this->load->view($view, $vars, true);
          $result .= $this->load->view('front/footer', $vars, true);

          return $result;
     } else {
          $this->load->view('front/header', $vars);
          $this->load->view($view, $vars);
          $this->load->view('front/footer', $vars);
     }
}

/*
This function simply calls $this->load->view()
*/

function partial($view, $vars = array(), $string = false) {
     if ($string) {
          return $this->load->view($view, $vars, true);
     } else {
          $this->load->view($view, $vars);
     }
}

}
