<?php

class Mailhelper
{

    public function enviar_email($to, $subject, $view, $data, $portal_ventas = false){

        $CI =& get_instance();

        $CI->load->library('email');

        $config = [
            'protocol' => 'smtp',
            'smtp_host' => 'mail.hqusistema.com.mx',
            'smtp_port' => 587,
            'smtp_user' => 'no-reply@hqusistema.com.mx',
            'smtp_pass' => '(wK%JQ~O8I6s',
            'mailtype'  => 'html',
            'charset'   => 'utf-8'
        ];

        $CI->email->initialize($config);

        $CI->email->from(
            'no-reply@hqusistema.com.mx',
            $portal_ventas ? 'HQu - Portal de Ventas' : 'HQu - Portal de Clientes'
        );

        $CI->email->to($to);
        $CI->email->subject($subject);
        $CI->email->message($CI->load->view($view, $data, true));

        return $CI->email->send();
    }


}