<?php

class Auth {
    public $_errores = '';


    public function __construct() {
        $this->load->library(array('email', 'session'));
        $this->lang->load('auth');
        $this->load->helper(array('language', 'url'));
        $this->load->model('auth_model');

        $email_config = array(
            'mailtype' => 'html'
        );

        $this->email->initialize($email_config);
    }

    /**
     * __call
     *
     * */
    public function __call($method, $arguments) {
        if (!method_exists($this->auth_model, $method)) {
            throw new Exception('Undefined method Auth::' . $method . '() called');
        }

        return call_user_func_array(array($this->auth_model, $method), $arguments);
    }

    /**
     * __get
     *
     * Enables the use of CI super-global without having to define an extra variable.
     *
     * @access	public
     * @param	$var
     * @return	mixed
     */
    public function __get($var) {
        return get_instance()->$var;
    }

   public function is_admin($id = FALSE) {
        $id OR $id = $this->session->userdata('usuario_id');

        if ($this->get_rol($id) == 'admin') {
            return TRUE;
        }

        return FALSE;

    }

    public function is_logged_in() {
        return (bool)$this->session->userdata('admin')['logged_in'];
    }
    public function is_logged_inCliente() {
        return (bool)$this->session->userdata('cliente')['logged_inCliente'];
    }

    public function recuperar_pass($email) {
        $usuario = $this->auth_model->get_usuario_by_email($email);
        if ($usuario) {
            if ($this->auth_model->generar_codigo_recuperar_pwd($email)) {
                return $this->enviar_mail_recuperar_pwd($email);
            } else {
                $this->_errores = 'Error en el proceso de recuperación de contraseña';
            }
        } else {
            $this->_errores = 'No existe ninguna cuenta asociada al correo ' . $email;
            return FALSE;
        }
    }

    private function enviar_mail_recuperar_pwd($email) {
        $config['protocol'] = 'Mailhelper';
        $config['charset'] = 'utf-8';
        $config['mailtype'] = 'html';
        $config['wordwrap'] = TRUE;

        $this->email->initialize($config);

        $usuario = $this->auth_model->get_usuario_by_email($email);

        if (!$usuario) {
            $this->_errores = 'No existe ninguna cuenta asociada al correo ' . $email;
            return FALSE;
        }

        $this->email->from('test@sumawebdesarrollo.com', 'APPCMS');
        $this->email->to($usuario->email);

        $this->email->subject('Recuperar contraseña | sitename');

        $data['sitio'] = 'sitename';
        $data['link'] = base_url('login/actualizar_password/' .  $usuario->username . '/' . $usuario->codigo_recuperar_pwd);
        $this->email->message($this->load->view('emails/recuperar_pass', $data, TRUE));

        if ($this->email->send()) {
            return TRUE;
        } else {
            $this->_errores = 'No fue posible enviar el correo con las instrucciones para restablecer su contraseña';
            return FALSE;
        }
    }

}
