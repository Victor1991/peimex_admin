<?php
class Relacionados_model extends CI_Model {

     public function get_relacionados($curso_id){
          return $this->db->from('recurso_relacionados_curso')->where('curso_id', $curso_id)->get()->result();
     }


     public function get_relacionados_id($recurso){
          return $this->db->from('recurso_relacionados_curso')->where('id', $recurso)->get()->row();
     }

     public function new_relacionados($data){
          return $this->db->insert('recurso_relacionados_curso',$data);
     }

     public function editar_relacionados($recurso, $data){
          $this->db->where('id', $recurso);
          return $this->db->update('recurso_relacionados_curso', $data);
     }

     public function delete_relacionados($recurso){
          $this->db->where('id', $recurso);
          return $this->db->delete('recurso_relacionados_curso');
     }




}
