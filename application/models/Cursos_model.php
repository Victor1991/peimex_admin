<?php
class Cursos_model extends CI_Model {

     public function get_curso_id($curso_id)
     {
          return $this->db->from('cursos')->where('id', $curso_id)->get()->row_array();
     }

     public function get_curso()
     {
          return $this->db->from('cursos')->get()->result();
     }

     public function params($params)
     {

          // if (!empty($params['activo'])) {
          //     $this->db->where('usuarios.activo', $params['activo']);
          // }



          if (!empty($params['buscar'])) {
               $this->db->like('nombre', trim($params['buscar']));
          }

          if ($this->rol_id_user == 2 || $this->rol_id_user == 3) {
               // _dump( $this->session->userdata('admin'));
               $this->db->group_start();
               $this->db->join('admin_cliente_curos cur_cli', 'cur_cli.curso_id = cursos.id','left');

               if ($this->rol_id_user == 2) {
                    $this->db->where('cur_cli.admin_id', $this->session->userdata('admin')['usuario_id']);
               }

               if ($this->rol_id_user == 3) {
                    $this->db->where('cur_cli.admin_id', $this->session->userdata('admin')['empresa_id']);
               }

               $this->db->group_end();
          }


     }

     public function get_cursos($params = [])
     {
          return [ 'count' => $this->count_by($params), 'data' => $this->get_all($params) ];
     }

     public function count_by($params)
     {
          $this->params($params);

          return $this->db->select("*", FALSE)
          ->from("cursos")
          ->count_all_results();
     }

     public function get_all($params) {

          $this->params($params);

          if (!empty($params['range'])) {
               $this->db->limit($params['range'][1], $params['range'][0]);
          }

          return $this->db->select("cursos.*", FALSE)
          ->from("cursos")
          ->order_by("cursos.id", "asc")
          ->get()
          ->result();
     }

     public function get_cursos_cliente($cliente_id){
          return $this->db->select("cursos.*", FALSE)
          ->from("cursos")
          ->join('admin_cliente_curos curso_clinte', 'curso_clinte.curso_id = cursos.id', 'left')
          ->where('curso_clinte.admin_id', $cliente_id)
          ->order_by("cursos.id", "asc")
          ->get()
          ->result();
     }

     public function new_curso($data){
          $this->db->insert('cursos',$data);
          return $this->db->insert_id();
     }

     public function registrar_curso_cliente($data){
          $this->db->insert('admin_cliente_curos',$data);
          return $this->db->insert_id();
     }

     public function edit_curso($curso_id, $data){
          $this->db->where('id', $curso_id);
          return $this->db->update('cursos', $data);
     }

     public function delete_curso($curso_id){
          $this->db->where('id', $curso_id);
          return $this->db->delete('cursos');
     }

     // Presentacion curso
     public function get_presentacion_curso($curso_id){
          return $this->db->from('material_introductorio')->where('curso_id', $curso_id)->get()->row();
     }

     public function insertar_presentacion($data){
          return $this->db->insert('material_introductorio',$data);
     }

     public function editar_presentacion($curso_id, $data){
          $this->db->where('id', $curso_id);
          return $this->db->update('material_introductorio', $data);
     }

     // Modulos cursos
     public function get_modulos_curoso($curso_id){
          $result = $this->db->from('secciones')->where('curso_id', $curso_id)->order_by('orden', 'asc')->get()->result();
          foreach ($result as $key => $value) {
               $value->materiales = $this->get_material_modulo($value->id);
          }
          return $result;
     }

     public function get_modulos_id($id){
          return $this->db->from('secciones')->where('id', $id)->get()->row();
     }


     public function insertar_modulo_curso($data){
          $this->db->insert('secciones',$data);
          return $this->db->insert_id();

     }

     public function editar_modulo_curso($id, $data){
          $this->db->where('id', $id);
          return $this->db->update('secciones', $data);
     }

     public function delete_modulos($modulo_id){
          $this->db->where('id', $modulo_id);
          return $this->db->delete('secciones');
     }


     // Materiales modulos
     public function get_material_modulo($modulo_id){
          return $this->db->from('materiales')->where('seccion_id', $modulo_id)->get()->result();
     }

     public function get_material_id($id){
          return $this->db->from('materiales')->where('id', $id)->get()->row();
     }

     public function insertar_material_modulo($data){
          $this->db->insert('materiales',$data);
          return $this->db->insert_id();
     }

     public function editar_material_modulo($id, $data){
          $this->db->where('id', $id);
          return $this->db->update('materiales', $data);
     }

     public function eliminar_material($id){
          $this->db->where('id', $id);
          return $this->db->delete('materiales');
     }

     public function estatus_cursos(){
          return $this->db->get('estatus_cursos')->result();
     }

     public function get_catalogo_preguntas(){
          return $this->db->get('catalogo_preguntas')->result();
     }

     public function get_categorias_cursos(){
          return $this->db->get('categorias_cursos')->result();
     }


}
