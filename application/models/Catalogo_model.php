<?php


class Catalogo_model extends CI_Model {

    public function get_paises($ayuda = false)
    {
        $opciones = [];

        $ayuda ? $opciones[''] = 'SELECCIONA UNO: ' : false;

        foreach ($this->db->order_by('nombre', 'asc')->get('pais')->result() as $paises) {
            $opciones[$paises->id] = $paises->nombre;
        }

        return $opciones;
    }


}
