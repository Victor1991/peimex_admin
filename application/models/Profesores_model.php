<?php
     class Profesores_model extends CI_Model {

     public function get_profesor($curso_id){
          return $this->db->from('medicos_participantes_cursos')->where('curso_id', $curso_id)->get()->result();
     }


     public function get_profesor_id($profesor_id){
          return $this->db->from('medicos_participantes_cursos')->where('id', $profesor_id)->get()->row();
     }

     public function new_profesor($data){
          return $this->db->insert('medicos_participantes_cursos',$data);
     }

     public function editar_profesor($profesor_id, $data){
          $this->db->where('id', $profesor_id);
          return $this->db->update('medicos_participantes_cursos', $data);
     }

     public function delete_profesor($profesor_id){
          $this->db->where('id', $profesor_id);
          return $this->db->delete('medicos_participantes_cursos');
     }




}
