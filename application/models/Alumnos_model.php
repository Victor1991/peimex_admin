<?php

class Alumnos_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function params($params)
    {
         if (!empty($params['buscar'])) {
              $this->db->group_start();
              $this->db->like('usuarios.correo', trim($params['buscar']))
              ->or_like('usuarios.nombre', trim($params['buscar']))
              ->or_like('usuarios.apellido_paterno', trim($params['buscar']))
              ->or_like('usuarios.apellidos_materno', trim($params['buscar']));
              $this->db->group_end(); // Close bracket

         }




        if ($this->rol_id_user == 2) {
             $this->db->where('alumnos.cliente_id', $this->session->userdata('admin')['usuario_id']);
        }


        if ($this->rol_id_user == 3) {
            $this->db->where('alumnos.cliente_id', $this->session->userdata('admin')['empresa_id']);
        }


        if (!empty($params['activo'])) {
             if ($params['activo'] == 1) {
                  $this->db->where('estatus', 1);
             }

             if ($params['activo'] == 2) {
                  $this->db->where('estatus', 0);
             }
        }

        if (!empty($params['ordernar_por'])) {
            $this->db->order_by("pedidos.fecha_creacion", $params['ordernar_por']);
        }

        if (!empty($params['empresa'])) {

           $this->db->where("alumnos.cliente_id", $params['empresa']);
        }

    }


     public function get_alumno_cliente($cliente_id){
          $this->db->select('usuarios.id');
          $this->db->from('alumnos');
          $this->db->join('usuarios', 'usuarios.id = alumnos.usuario_id');
          $this->db->where('cliente_id', $cliente_id);
          return $this->db->get()->result();
     }

     public function get_intentos_alumno_curso($usuario_id, $curso){
          return $this->db->where('usuario_id', $usuario_id)->where('curso_id', $curso)->get('intentos')->result();
     }


    public function get_alumnos($params = [])
    {
        return [ 'count' => $this->count_by($params), 'data' => $this->get_all($params) ];
    }

    public function count_by($params)
    {
        $this->params($params);

        return $this->db->select("usuarios.*, CONCAT(nombre, ' ', apellido_paterno , ' ', apellidos_materno) as nombre_completo", FALSE)
            ->from("usuarios")
            ->join("alumnos", "alumnos.usuario_id = usuarios.id", 'left')
            ->group_by("usuarios.id")
            ->count_all_results();
    }

    public function get_all($params) {

        $this->params($params);

        if (!empty($params['range'])) {
            $this->db->limit($params['range'][1], $params['range'][0]);
       }else {
            $this->db->limit(20, 0);
       }

        return $this->db->select(' usuarios.*,  administradores.username as empresa ', FALSE)
            ->from("usuarios")
            ->join("alumnos", "alumnos.usuario_id = usuarios.id", 'left')
            ->join("administradores", "administradores.id = alumnos.cliente_id", 'left')
            ->order_by("usuarios.id", "desc")
            ->group_by("usuarios.id")
            ->get()
            ->result();
    }

    public function alumnos_curso($curso_id, $type = false){

          $aprobado = $this->db->where('intentos.aprobado', 1)->where('intentos.curso_id', $curso_id)->get('intentos')->result();
          $id_aprobados = array_column($aprobado, 'usuario_id');

         $this->db->select("usuarios.*, CONCAT(nombre, ' ', apellido_paterno , ' ', apellidos_materno) as nombre_completo", FALSE)
         ->from("usuarios")
         ->join('alumnos_cursos', 'alumnos_cursos.usuario_id = usuarios.id', 'LEFT')
         ->join('intentos', 'intentos.usuario_id = usuarios.id', 'LEFT')
         ->where('alumnos_cursos.curso_id', $curso_id)
         ->where('intentos.curso_id', $curso_id);
         if ($type == 1) {
              $this->db->where('intentos.aprobado', 1);
         }

         if ($type == 2) {
              $this->db->where('intentos.aprobado', 0);
              if ($id_aprobados) {
                   $this->db->where_not_in('usuarios.id',$id_aprobados);
              }
         }

         if ($this->rol_id_user == 2) {
              $this->db->join('alumnos','alumnos.usuario_id = usuarios.id', 'right');
              $this->db->where('alumnos.cliente_id', $this->session->userdata('admin')['usuario_id']);
         }

         if ($this->rol_id_user == 3) {
             $this->db->join('alumnos','alumnos.usuario_id = usuarios.id', 'right');
             $this->db->where('alumnos.cliente_id', $this->session->userdata('admin')['empresa_id']);
         }

         $this->db->where('usuarios.id IS NOT NULL');
         $this->db->group_by('usuarios.id');
         return $this->db->count_all_results();
    }

    /*public function get_many_by($params = array(), $limit = FALSE, $start = 0) {
        if (!empty($params['activo'])) {
            $params['activo'] = $params['activo'] === 2 ? 0 : $params['activo'];
            $this->db->where('usuarios.activo', $params['activo']);
        }

        if (!empty($params['rol_id'])) {
            $this->db->where('usuarios.rol_id', $params['rol_id']);
        }

        if (!empty($params['buscar'])) {
            $this->db->like('usuarios.username', trim($params['buscar']))
                    ->or_like('usuarios.email', trim($params['buscar']))
                    ->or_like('usuarios.nombre', trim($params['buscar']))
                    ->or_like('usuarios.apellidos', trim($params['buscar']));
        }

        if ($limit) {
            $this->db->limit($limit, $start);
        }

        return $this->get_all();
    }*/


    /*public function get_autorerrores() {
        $this->db->select("usuarios.id, usuarios.nombre, usuarios.apellidos, CONCAT(usuarios.nombre, ' ', usuarios.apellidos) as nombre_completo, "
                . "usuarios.rol_id, roles.nombre as rol_nombre", FALSE);
        $this->db->from("usuarios");
        $this->db->join("roles", "roles.id = usuarios.rol_id", "inner");
        $this->db->join("roles_permisos", "roles_permisos.rol_id = roles.id", "inner");
        $this->db->join("permisos", "permisos.id = roles_permisos.permiso_id", "inner");
        $this->db->where("permisos.clave", "crear_posts");
        $this->db->where("roles_permisos.valor", 1);
        $this->db->order_by("nombre_completo");
        $this->db->group_by("usuarios.id");

        return $this->db->get()->result();
    }*/


    public function get_by_id($id, $array = 'false') {
         $query = $this->db->select("*", FALSE)
         ->from("usuarios")
         ->join("alumnos", "alumnos.usuario_id = usuarios.id", "left")
         ->where("usuarios.id", $id)
         ->get();
         if ($array) {
              return $query->row_array();
         }else{
              return $query->row();
         }
    }

    public function alumnos_curso_inscrito($alumno_id){
         return $this->db->select('*', FALSE)
            ->from("alumnos_cursos")
            ->get()
            ->result();
    }

    public function validar_alumno_in_curso($alumno_id,$curso_id){
         return $this->db->select('*', FALSE)
            ->from("alumnos_cursos")
            ->where('usuario_id', $alumno_id)
            ->where('curso_id', $curso_id)
            ->get()
            ->row();
    }

    public function inser_alumnos_curso_inscrito($data){
         $this->db->insert('alumnos_cursos', $data);
         return $this->db->insert_id();
    }

    public function eliminar_alumnos_curso_inscrito($alumno_id,$curso_id){
         return $this->db->delete('alumnos_cursos', array('usuario_id' => $alumno_id, 'curso_id' => $curso_id));

    }

    public function get_by_correo($correo){
         return $this->db->select('*', FALSE)
            ->from("usuarios")
            ->where('correo', $correo)
            ->get()
            ->row();
    }

    public function insert($data) {
        $this->db->insert('usuarios', $data);
        return $this->db->insert_id();
    }

    public function update($id, $data) {
        return $this->db->update('usuarios', $data, array('id' => $id));
    }

    public function delete($id) {
        return $this->db->delete('usuarios', array('id' => $id));
    }

    public function eliminar_insertar_alumno($data, $usuario_id)
    {
        if ($usuario_id) {
             $this->db->delete('alumnos', array('usuario_id' => $usuario_id));
        }

        $this->db->insert('alumnos', $data);
        return $this->db->insert_id();
    }

    public function insertar_alumno($data){
         $this->db->insert('alumnos', $data);
         return $this->db->insert_id();
    }

    public function update_alumno($alumno_id, $data) {
        return $this->db->update('alumnos', $data, array('usuario_id' => $alumno_id));
    }

    public function get_alumnos_by_user_id($user_id)
    {
         return $this->db->from("alumnos")->where('usuario_id', $user_id)->get()->row();
    }

    // Inputs
    public function get_input_alumno($alumno_id){
         return $this->db->select('*', FALSE)
            ->from("inputs_alumnos")
            ->where('alumno_id', $alumno_id)
            ->get()
            ->result();
    }

    public function get_input_id($id){
         return $this->db->select('*', FALSE)
           ->from("inputs_alumnos")
           ->where('id', $id)
           ->get()
           ->result();
    }

    public function insert_input($data){
         $this->db->insert('inputs_alumnos', $data);
         return $this->db->insert_id();
    }

    public function update_input($id, $data){
         return $this->db->update('inputs_alumnos', $data, array('id' => $id));

    }

    public function delete_input($id){
         return $this->db->delete('inputs_alumnos', array('id' => $id));
    }

     public function get_alumnos_curso($curso_id){
          $this->db->select('usuarios.*, intentos.*, intentos.id as intento_id, cursos.nombre as curso, administradores.username as cliente,
          max(intentos.num_intento) as num_intento,
          max(intentos.inicio) as inicio,
          max(intentos.fin) as fin,
          max(intentos.calificacion) as calificacion
          ');
          $this->db->from('intentos');
          $this->db->join('usuarios','usuarios.id = intentos.usuario_id', 'LEFT');
          $this->db->join('cursos','cursos.id = intentos.curso_id', 'left');
          $this->db->join('alumnos','alumnos.usuario_id = usuarios.id', 'RIGHT');
          $this->db->join('administradores','administradores.id = alumnos.cliente_id', 'LEFT');
          $this->db->where('usuarios.id IS NOT NULL');

          if ($this->rol_id_user == 2) {
               $this->db->where('alumnos.cliente_id', $this->session->userdata('admin')['usuario_id']);
          }

          if ($this->rol_id_user == 3) {
               $this->db->where('alumnos.cliente_id', $this->session->userdata('admin')['empresa_id']);
          }

          $this->db->where('intentos.curso_id', $curso_id);
          $this->db->group_by('usuarios.id');
          $this->db->order_by('intentos.aprobado', 'desc');
          return $this->db->get()->result();

     }

}
