<?php
class Preguntas_model extends CI_Model {


     // Preguntas materiales
     public function get_preguntas_materiales($materil_id){
          $return =  $this->db->from('preguntas')->where('material_id', $materil_id)->order_by('orden', 'asc')->get()->result();
          foreach ($return as $key => $value) {
               $value->respuestas = $this->get_respuestas_pregunta($value->id);
          }
          return $return;
     }

     public function get_preguntas_examen($examen_id){
          $return =  $this->db->from('preguntas')->where('examen_id', $examen_id)->order_by('orden', 'asc')->get()->result();
          foreach ($return as $key => $value) {
               if ($value->catalogo_preguntas == 1 || $value->catalogo_preguntas == 4 || $value->catalogo_preguntas == 6 || $value->catalogo_preguntas == 5 ) {
                    $value->respuestas = $this->get_respuestas_pregunta($value->id);
               }
               if ($value->catalogo_preguntas == 2 || $value->catalogo_preguntas == 3) {
                    $value->pregunta_hijo = $this->db->from('preguntas')->where('pregunta_padre', $value->id)->get()->result();
                    foreach ($value->pregunta_hijo as $key2 => $pregunta) {
                         $value->pregunta_hijo[$key2]->respuestas = $this->get_respuestas_pregunta($pregunta->id);
                    }
               }
               if ($value->catalogo_preguntas == 3) {
                    // code...
               }
          }

          // _dump($return);

          return $return;
     }

     public function get_pregunta_id($id){
          return $this->db->from('preguntas')->where('id', $id)->get()->row();
     }

     public function insertar_pregunta($data){
          unset($data['respuestas']);
          $this->db->insert('preguntas',$data);
          return $this->db->insert_id();
     }


     public function editar_pregunta($id, $data){
          $this->db->where('id', $id);
          return $this->db->update('preguntas', $data);
     }

     public function eliminar_pregunta($id){
          $this->db->where('id', $id);
          return $this->db->delete('preguntas');
     }

     public function get_respuesta_alumno($pregunta_id, $intento_id){
          $this->db->where('pregunta_id', $pregunta_id);
          $this->db->where('intento_id', $intento_id);
          return $this->db->get('respuestas_alumno')->row();
     }

     // Get respuestas preguntas
     public function get_respuestas_pregunta($pregunta_id){
          return $this->db->from('respuestas_preguntas')->where('pregunta_id', $pregunta_id)->get()->result();
     }

     public function insert_bach($data) {
          return $this->db->insert_batch('respuestas_preguntas', $data);
     }

     public function insertar_respuesta($data){
          $this->db->insert('respuestas_preguntas',$data);
          return $this->db->insert_id();
     }


     public function editar_respuesta($id, $data){
          $this->db->where('id', $id);
          return $this->db->update('respuestas_preguntas', $data);
     }

     public function eliminar_respuestas($no_eliminar = array(), $pregunta_id){
          $this->db->where_not_in('id', $no_eliminar);
          $this->db->where('pregunta_id', $pregunta_id);
          return $this->db->delete('respuestas_preguntas');
     }

     public function eliminar_respuesta($id){
          $this->db->where('id', $id);
          return $this->db->delete('respuestas_preguntas');
     }

     public function eliminar_preguntas($no_eliminar = array(), $pregunta_id){
          $this->db->where_not_in('id', $no_eliminar);
          $this->db->where('pregunta_padre', $pregunta_id);
          return $this->db->delete('preguntas');
     }



}
