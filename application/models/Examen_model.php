<?php
class Examen_model extends CI_Model {

     public function get_examen($curso_id, $type){
          return $this->db->from('examen')->where('curso_id', $curso_id)->where('tipo', $type)->get()->row();
     }


     public function get_examen_id($examen_id){
          return $this->db->from('examen')->where('id', $examen_id)->get()->row();
     }

     public function new_examen($data){
          $this->db->insert('examen',$data);
          return $this->db->insert_id();
     }

     public function editar_examen($examen_id, $data){
          $this->db->where('id', $examen_id);
          return $this->db->update('examen', $data);
     }

     public function delete_examen($examen_id){
          $this->db->where('id', $examen_id);
          return $this->db->delete('examen');
     }

     public function get_examenes_curso($curso_id)
     {
          return $this->db->from('examen')->where('curso_id', $curso_id)->get()->result();
     }




}
