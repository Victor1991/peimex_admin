<?php

class Administrador_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

   public function params($params)
   {


       if (!empty($params['activo'])) {
           $params['activo'] = $params['activo'] === 2 ? 0 : $params['activo'];
           $this->db->where('administradores.activo', $params['activo'] == 2 ? 0 :  $params['activo'] );
       }

       if (!empty($params['rol_id'])) {
           $this->db->where_in('administradores.rol_id', $params['rol_id']);
       }

       if (!empty($params['buscar'])) {
           $this->db->like('administradores.username', trim($params['buscar']))
               ->or_like('administradores.email', trim($params['buscar']))
               ->or_like('administradores.nombre', trim($params['buscar']))
               ->or_like('administradores.apellidos', trim($params['buscar']));
       }

       if (!empty($params['ordernar_por'])) {
           $this->db->order_by("pedidos.fecha_creacion", "desc");
       }
   }

   public function get_usuarios($params = [])
   {
       return [ 'count' => $this->count_by($params), 'data' => $this->get_all($params) ];
   }

   public function count_by($params)
   {
       $this->params($params);

       return $this->db->select("administradores.*, CONCAT(administradores.nombre, ' ', administradores.apellidos) as nombre_completo, roles.nombre as rol_nombre", FALSE)
           ->from("administradores")
           ->join("roles", "roles.id = administradores.rol_id", "inner")
           ->count_all_results();
   }

   public function get_all($params) {

       $this->params($params);

       if (!empty($params['range'])) {
           $this->db->limit($params['range'][1], $params['range'][0]);
       }

        $users = $this->db->select("administradores.*, CONCAT(administradores.nombre, ' ', administradores.apellidos) as nombre_completo, roles.nombre as rol_nombre", FALSE)
           ->from("administradores")
           ->join("roles", "roles.id = administradores.rol_id", "inner")
           ->order_by("administradores.nombre, administradores.apellidos", "asc")
           ->get()
           ->result();

          foreach ($users as $key => $user) {
               if ($user->empresa_id) {
                    $user->empresa = $this->get_user_id($user->empresa_id);
               }
          }
          return $users;
   }

     public function get_user_id($id)
     {
          return $this->db->where('id', $id)->get('administradores')->row();
     }

   /*public function get_many_by($params = array(), $limit = FALSE, $start = 0) {
       if (!empty($params['activo'])) {
           $params['activo'] = $params['activo'] === 2 ? 0 : $params['activo'];
           $this->db->where('administradores.activo', $params['activo']);
       }

       if (!empty($params['rol_id'])) {
           $this->db->where('administradores.rol_id', $params['rol_id']);
       }

       if (!empty($params['buscar'])) {
           $this->db->like('administradores.username', trim($params['buscar']))
                   ->or_like('administradores.email', trim($params['buscar']))
                   ->or_like('administradores.nombre', trim($params['buscar']))
                   ->or_like('administradores.apellidos', trim($params['buscar']));
       }

       if ($limit) {
           $this->db->limit($limit, $start);
       }

       return $this->get_all();
   }*/


   /*public function get_autorerrores() {
       $this->db->select("administradores.id, administradores.nombre, administradores.apellidos, CONCAT(administradores.nombre, ' ', administradores.apellidos) as nombre_completo, "
               . "administradores.rol_id, roles.nombre as rol_nombre", FALSE);
       $this->db->from("administradores");
       $this->db->join("roles", "roles.id = administradores.rol_id", "inner");
       $this->db->join("roles_permisos", "roles_permisos.rol_id = roles.id", "inner");
       $this->db->join("permisos", "permisos.id = roles_permisos.permiso_id", "inner");
       $this->db->where("permisos.clave", "crear_posts");
       $this->db->where("roles_permisos.valor", 1);
       $this->db->order_by("nombre_completo");
       $this->db->group_by("administradores.id");

       return $this->db->get()->result();
   }*/

   public function getVendedores() {
       $this->db->select("administradores.id, administradores.nombre, administradores.apellidos, CONCAT(administradores.nombre, ' ', administradores.apellidos) as nombre_completo, "
           . "administradores.rol_id, roles.nombre as rol_nombre", FALSE);
       $this->db->from("administradores");
       $this->db->join("roles", "roles.id = administradores.rol_id", "inner");
       $this->db->join("roles_permisos", "roles_permisos.rol_id = roles.id", "left");
       $this->db->join("permisos", "permisos.id = roles_permisos.permiso_id", "left");
       $this->db->where("roles.clave", "admin");
       $this->db->or_where("roles.clave", "sup_ventas");
       $this->db->or_where("roles.clave", "ventas");
       $this->db->order_by("nombre_completo");

       $opciones = [];
       $opciones = array('' => 'SELECCIONA UNO:');

       $vendedores = $this->db->get()->result();
       if ($vendedores) {
           foreach ($vendedores as $vendedor) {
               $opciones[$vendedor->id] = $vendedor->nombre_completo;
           }
       }

       return $opciones;
   }

   public function get_by_id($id) {
       $query = $this->db->select("administradores.*, CONCAT(administradores.nombre, ' ', administradores.apellidos) as nombre_completo", FALSE)
           ->from("administradores")
           ->where("id", $id)
           ->get();

       return $query->row();
   }

   public function insert($data) {
       $data['fecha_creacion'] = date('Y-m-d H:i:s');
       $data['fecha_edicion'] = $data['fecha_creacion'];

       $this->db->insert('administradores', $data);

       return $this->db->insert_id();
   }

   public function update($id, $data) {
       return $this->db->update('administradores', $data, array('id' => $id));
   }

   public function delete($id) {
       return $this->db->delete('administradores', array('id' => $id));
   }


   public function validar_cliente_in_curso($cliente_id,$curso_id){
        return $this->db->select('*', FALSE)
          ->from("admin_cliente_curos")
          ->where('admin_id', $cliente_id)
          ->where('curso_id', $curso_id)
          ->get()
          ->row();
   }

   public function inser_cliente_curso_inscrito($data){
        $this->db->insert('admin_cliente_curos', $data);
        return $this->db->insert_id();
   }

   public function eliminar_cliente_curso_inscrito($cliente_id,$curso_id){
        return $this->db->delete('admin_cliente_curos', array('admin_id' => $cliente_id, 'curso_id' => $curso_id));

   }



}
