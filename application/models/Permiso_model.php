<?php

class Permiso_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    public function get_permisos() {
        return $this->db->get('permisos')->result();
    }
    
    public function get_permisos_rol($rol_id) {
        return $this->db->select('roles_permisos.*, permisos.clave, permisos.modulo')
                ->from('roles_permisos')
                ->join('permisos', 'roles_permisos.permiso_id = permisos.id', 'inner')
                ->order_by('permisos.orden', 'asc')
                ->get()
                ->result();
    }
    
    public function get_permisos_modulo($modulo) {
        return $this->db->where('modulo', $modulo)->order_by('permisos.orden', 'asc')->get('permisos')->result();
    }
    
}