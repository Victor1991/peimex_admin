<?php

class Institucion_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function buscar_institucion_nombre($nombre) {
        return $this->db->like('nombre', $nombre)->get('instituciones')->row();
    }

    public function crear_instirucion($data){
         $this->db->insert('instituciones',$data);
         return $this->db->insert_id();
    }

     public function editar_institucion($id, $data){
          $this->db->where('id', $id);
          return $this->db->update('instituciones', $data);
     }
}
