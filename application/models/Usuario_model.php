<?php

class Usuario_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function params($params)
    {

        if (!empty($params['activo'])) {
            $params['activo'] = $params['activo'] === 2 ? 0 : $params['activo'];
            $this->db->where('usuarios.activo', $params['activo']);
        }

        if (!empty($params['rol_id'])) {
            $this->db->where_in('usuarios.rol_id', $params['rol_id']);
        }

        if (!empty($params['buscar'])) {
            $this->db->like('usuarios.username', trim($params['buscar']))
                ->or_like('usuarios.email', trim($params['buscar']))
                ->or_like('usuarios.nombre', trim($params['buscar']))
                ->or_like('usuarios.apellidos', trim($params['buscar']));
        }

        if (!empty($params['ordernar_por'])) {
            $this->db->order_by("pedidos.fecha_creacion", "desc");
        }
    }

    public function get_usuarios($params = [])
    {
        return [ 'count' => $this->count_by($params), 'data' => $this->get_all($params) ];
    }

    public function count_by($params)
    {
        $this->params($params);

        return $this->db->select("usuarios.*, CONCAT(usuarios.nombre, ' ', usuarios.apellidos) as nombre_completo, roles.nombre as rol_nombre", FALSE)
            ->from("usuarios")
            ->join("roles", "roles.id = usuarios.rol_id", "inner")
            ->count_all_results();
    }

    public function get_all($params) {

        $this->params($params);

        if (!empty($params['range'])) {
            $this->db->limit($params['range'][1], $params['range'][0]);
        }

        return $this->db->select("usuarios.*, CONCAT(usuarios.nombre, ' ', usuarios.apellido_paterno , ' ', usuarios.apellidos_materno) as nombre_completo, roles.nombre as rol_nombre", FALSE)
            ->from("usuarios")
            ->join("roles", "roles.id = usuarios.rol_id", "inner")
            ->order_by("usuarios.nombre, usuarios.apellidos", "asc")
            ->get()
            ->result();
    }

    /*public function get_many_by($params = array(), $limit = FALSE, $start = 0) {
        if (!empty($params['activo'])) {
            $params['activo'] = $params['activo'] === 2 ? 0 : $params['activo'];
            $this->db->where('usuarios.activo', $params['activo']);
        }

        if (!empty($params['rol_id'])) {
            $this->db->where('usuarios.rol_id', $params['rol_id']);
        }

        if (!empty($params['buscar'])) {
            $this->db->like('usuarios.username', trim($params['buscar']))
                    ->or_like('usuarios.email', trim($params['buscar']))
                    ->or_like('usuarios.nombre', trim($params['buscar']))
                    ->or_like('usuarios.apellidos', trim($params['buscar']));
        }

        if ($limit) {
            $this->db->limit($limit, $start);
        }

        return $this->get_all();
    }*/


    /*public function get_autorerrores() {
        $this->db->select("usuarios.id, usuarios.nombre, usuarios.apellidos, CONCAT(usuarios.nombre, ' ', usuarios.apellidos) as nombre_completo, "
                . "usuarios.rol_id, roles.nombre as rol_nombre", FALSE);
        $this->db->from("usuarios");
        $this->db->join("roles", "roles.id = usuarios.rol_id", "inner");
        $this->db->join("roles_permisos", "roles_permisos.rol_id = roles.id", "inner");
        $this->db->join("permisos", "permisos.id = roles_permisos.permiso_id", "inner");
        $this->db->where("permisos.clave", "crear_posts");
        $this->db->where("roles_permisos.valor", 1);
        $this->db->order_by("nombre_completo");
        $this->db->group_by("usuarios.id");

        return $this->db->get()->result();
    }*/

    public function getVendedores() {
        $this->db->select("usuarios.id, usuarios.nombre, usuarios.apellidos, CONCAT(usuarios.nombre, ' ', usuarios.apellidos) as nombre_completo, "
            . "usuarios.rol_id, roles.nombre as rol_nombre", FALSE);
        $this->db->from("usuarios");
        $this->db->join("roles", "roles.id = usuarios.rol_id", "inner");
        $this->db->join("roles_permisos", "roles_permisos.rol_id = roles.id", "left");
        $this->db->join("permisos", "permisos.id = roles_permisos.permiso_id", "left");
        $this->db->where("roles.clave", "admin");
        $this->db->or_where("roles.clave", "sup_ventas");
        $this->db->or_where("roles.clave", "ventas");
        $this->db->order_by("nombre_completo");

        $opciones = [];
        $opciones = array('' => 'SELECCIONA UNO:');

        $vendedores = $this->db->get()->result();
        if ($vendedores) {
            foreach ($vendedores as $vendedor) {
                $opciones[$vendedor->id] = $vendedor->nombre_completo;
            }
        }

        return $opciones;
    }

    public function get_by_id($id) {
        $query = $this->db->select("usuarios.*, CONCAT(usuarios.nombre, ' ', usuarios.apellido_paterno , ' ', usuarios.apellidos_materno) as nombre_completo", FALSE)
            ->from("usuarios")
            ->where("id", $id)
            ->get();

        return $query->row();
    }

    public function insert($data) {
        $data['fecha_creacion'] = date('Y-m-d H:i:s');
        $data['fecha_edicion'] = $data['fecha_creacion'];

        $this->db->insert('usuarios', $data);

        return $this->db->insert_id();
    }

    public function update($id, $data) {
        return $this->db->update('usuarios', $data, array('id' => $id));
    }

    public function delete($id) {
        return $this->db->delete('usuarios', array('id' => $id));
    }

}
