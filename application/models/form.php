<div class="row" id="app">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Listado de roles</h5>
            </div>
            <div class="ibox-content">

                <form-roles
                        :prop_rol='<?=  htmlspecialchars(json_encode($rol), ENT_QUOTES, 'UTF-8'); ?>'
                        :prop_permisos_rol='<?=  htmlspecialchars(json_encode($permisos_rol), ENT_QUOTES, 'UTF-8'); ?>'
                        :prop_permisos='<?=  htmlspecialchars(json_encode($permisos), ENT_QUOTES, 'UTF-8'); ?>'>
                </form-roles>

            </div>
        </div>
    </div>
</div>
