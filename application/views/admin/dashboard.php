<div class="content">

     <div class="container-fluid">
          <div class="row">


               <?php if($this->rol_id_user == 1): ?>

               <div class="col-lg-4 col-md-4 col-sm-6">

                    <div class="card card-stats">
                         <div class="card-header card-header-info card-header-icon">

                              <div class="card-icon" style="cursor: pointer;" onclick="window.location='<?=base_url('usuarios')?>';">
                                   <i class="material-icons">person</i>
                              </div>

                              <p class="card-category">Adminis.</p>
                              <h3 class="card-title"><?=$usuarios?><br>
                                   <small>Registrado(s)</small>
                              </h3>
                         </div>

                    </div>
               </div>
          <?php endif; ?>

               <div class="col-lg-4 col-md-4 col-sm-6">

                    <div class="card card-stats">
                         <div class="card-header card-header-info card-header-icon">
                              <div class="card-icon" style="cursor: pointer;" onclick="window.location='<?=base_url('cursos')?>';">
                                   <i class="material-icons">local_library</i>
                              </div>
                              <p class="card-category">Cursos</p>
                              <h3 class="card-title"><?=$cursos?><br>
                                   <small>Creados(s)</small>
                              </h3>
                         </div>

                    </div>
               </div>

               <div class="col-lg-4 col-md-4 col-sm-6">

                    <div class="card card-stats">
                         <div class="card-header card-header-info card-header-icon">
                              <div class="card-icon" style="cursor: pointer;" onclick="window.location='<?=base_url('alumnos')?>';">
                                   <i class="material-icons">school</i>
                              </div>
                              <p class="card-category">Participantes</p>
                              <h3 class="card-title"><?=$alumnos?><br>
                                   <small>Registrado(s)</small>
                              </h3>
                         </div>

                    </div>
               </div>

          </div>
     </div>
</div>
