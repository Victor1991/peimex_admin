<!DOCTYPE html>
<html lang="es">

<head>
     <meta charset="utf-8" />
     <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
     <link rel="icon" type="image/png" href="../assets/img/favicon.png">
     <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
     <title>
          Sistema
     </title>
     <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
     <!--     Fonts and icons     -->
     <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
     <!-- CSS Files -->
     <link href="<?=base_url()?>/assets/css/material-dashboard.css" rel="stylesheet" />
     <!-- CSS Just for demo purpose, don't include it in your project -->
     <link href="<?=base_url()?>/assets/demo/demo.css" rel="stylesheet" />
     <link href="<?=base_url()?>/assets/css/material-full.css" rel="stylesheet" />
     <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet" />


</head>
<!-- <body class="sidebar-mini"> -->
<body class="" id="body">
     <div class="wrapper ">
          <div class="sidebar" data-color="azure" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
               <div class="logo">
                    <a class="simple-text logo-mini">
                         <img src="<?=base_url('assets/img/peimxe-logo.png')?>" style="max-width:100%;" alt="">
                    </a>
                         <a class="simple-text logo-normal">
                         CapaPEI
                    </a>
               </div>
               <div class="sidebar-wrapper">
                    <ul class="nav">
                         <li class="nav-item  <?php if ($this->modulo == 1): ?> active <?php endif; ?> ">
                              <a class="nav-link" href="<?=base_url()?>">
                                   <i class="material-icons">dashboard</i>
                                   <p>Panel de control</p>
                              </a>
                         </li>
                         <?php if($this->rol_id_user == 1 ): ?>
                              <li class="nav-item  <?php if ($this->modulo == 2): ?> active <?php endif; ?> ">
                                   <a class="nav-link" href="<?=base_url('/usuarios')?>">
                                        <i class="material-icons">person</i>
                                        <p>Administradores</p>
                                   </a>
                              </li>
                         <?php endif; ?>
                         <!-- <li class="nav-item  <?php if ($this->modulo == 3): ?> active <?php endif; ?> ">
                              <a class="nav-link" href="<?=base_url('/roles')?>">
                                   <i class="material-icons">tune</i>
                                   <p>Roles</p>
                              </a>
                         </li> -->

                         <li class="nav-item  <?php if ($this->modulo == 4): ?> active <?php endif; ?> ">
                              <a class="nav-link" href="<?=base_url('/cursos')?>">
                                   <i class="material-icons">local_library</i>
                                   <p>Cursos</p>
                              </a>
                         </li>
                         <li class="nav-item  <?php if ($this->modulo == 5): ?> active <?php endif; ?> ">
                              <a class="nav-link" href="<?=base_url('/alumnos')?>">
                                   <i class="material-icons">school</i>
                                   <p>Participantes</p>
                              </a>
                         </li>
                         <li class="nav-item">
                              <a class="nav-link" href="<?=base_url('login/logout')?>">
                                   <i class="material-icons">exit_to_app</i>
                                   <p>Cerrar sesión</p>
                              </a>
                         </li>
                    </ul>
               </div>
          </div>

          <div class="main-panel" >
               <!-- Navbar -->
               <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
                    <div class="container-fluid">

                         <div class="navbar-wrapper">
                              <div class="navbar-minimize">
                                   <button id="minimizeSidebar" class="btn btn-just-icon btn-white btn-fab btn-round">
                                        <i class="material-icons text_align-center visible-on-sidebar-regular">more_vert</i>
                                        <i class="material-icons design_bullet-list-67 visible-on-sidebar-mini">view_list</i>
                                        <div class="ripple-container"></div>
                                   </button>
                              </div>
                              <a class="navbar-brand" ><?=$this->nombre_modulo ?></a>
                         </div>

                         <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                              <span class="sr-only">Toggle navigation</span>
                              <span class="navbar-toggler-icon icon-bar"></span>
                              <span class="navbar-toggler-icon icon-bar"></span>
                              <span class="navbar-toggler-icon icon-bar"></span>
                         </button>
                         <div class="collapse navbar-collapse justify-content-end">
                              <ul class="navbar-nav">
                                   <li class="nav-item dropdown">
                                        <a class="nav-link btn-new"  id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                             <i class="material-icons">person</i>
                                             <p class="d-lg-none d-md-block">
                                                  Cuenta
                                             </p>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                                             <div class="dropdown-divider"></div>
                                             <a class="dropdown-item" href="<?=base_url('login/logout')?>">Salir</a>
                                        </div>
                                   </li>
                              </ul>
                         </div>
                    </div>
               </nav>
               <!-- End Navbar -->
               <div class="content" id="app">
                    <div class="container-fluid">
