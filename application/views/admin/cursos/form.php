<div class="card card-nav-tabs">
     <div class="card-header card-header-info">
          <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
          <div class="nav-tabs-navigation">
               <div class="nav-tabs-wrapper">
                    <ul class="nav nav-tabs" data-tabs="tabs">
                         <li class="nav-item">
                              <a class="nav-link active show" href="#tab_profile" data-toggle="tab">
                                   <i class="material-icons">school</i> <?=$form['id'] ? 'Editar portada curso' : 'Portada curso' ;?>
                              </a>
                         </li>
                         <?php if ( $form['id'] && $this->rol_id_user == 1  ): ?>
                              <li class="nav-item">
                                   <a class="nav-link " href="#tab_cursos" data-toggle="tab">
                                        <i class="material-icons">local_library</i> Cursos inscrito
                                   </a>
                              </li>
                         <?php endif; ?>

                    </ul>
               </div>
          </div>
     </div>
     <div class="card-body ">
          <div class="tab-content ">
               <div class="tab-pane active show" id="tab_profile">
                    <br>
                    <div class="col-md-12">
                         <?php show_alerts(); ?>
                         <br>
                         <formulario-cursos
                                   :informacion="<?= htmlspecialchars(json_encode($form), ENT_QUOTES, 'UTF-8'); ?>"
                                   :estatus="<?= htmlspecialchars(json_encode($estatus), ENT_QUOTES, 'UTF-8'); ?>"
                                   :categorias="<?= htmlspecialchars(json_encode($categorias), ENT_QUOTES, 'UTF-8'); ?>"
                         ></formulario-cursos>
                    </div>
               </div>
               <div class="tab-pane"  id="tab_cursos">
                    <formulario-clientes-cursos :curso_id="<?= htmlspecialchars(json_encode($form['id']), ENT_QUOTES, 'UTF-8'); ?>"></formulario-clientes-cursos>
               </div>

          </div>
     </div>
</div>
<modal-upload></modal-upload>
<modal-upload-small></modal-upload-small>
