<div class="row">
     <div class="col-md-12">
          <div class="card">
               <div class="card-header card-header-info">
                    <h4 class="card-title ">
                         Cursos
                         <?php if ($_SESSION['admin']['rol_id'] == 1):?>
                         <a href="<?=base_url('cursos/form')?>" class="btn btn-sm btn-warning pull-right"> <i class="fa fa-plus"></i> </a>
                    <?php endif; ?>
                    </h4>
               </div>
               <div class="card-body">
                    <div class="col-md-12">
                         <filtros-tabla-cursos  :filtros="<?= htmlspecialchars(json_encode($filtos), ENT_QUOTES, 'UTF-8'); ?>" ></filtros-tabla-cursos>
                         <hr>
                         <tabla-cursos :dir_archives="<?= htmlspecialchars(json_encode(DIR_ARCHIVES), ENT_QUOTES, 'UTF-8'); ?>"></tabla-cursos >
                    </div>
               </div>
          </div>
     </div>
</div>
