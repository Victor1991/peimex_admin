<div class="content">

     <div class="container-fluid">
          <div class="row">
               <div class="col-lg-12">


                    <div class="card card-nav-tabs">
                         <div class="card-header card-header-info">
                              <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
                              <div class="nav-tabs-navigation">
                                   <div class="nav-tabs-wrapper">
                                        <ul class="nav nav-tabs" data-tabs="tabs">
                                             <li class="nav-item">
                                                  <a class="nav-link active show" href="#prese" data-toggle="tab">
                                                       <i class="material-icons">art_track</i> Presentación
                                                  </a>
                                             </li>
                                             <li class="nav-item">
                                                  <a class="nav-link " href="#modul" data-toggle="tab">
                                                       <i class="material-icons">burst_mode</i> Módulos
                                                       <div class="ripple-container"></div>
                                                  </a>
                                             </li>
                                             <li class="nav-item">
                                                  <a class="nav-link" href="#exam" data-toggle="tab">
                                                       <i class="material-icons">check_circle</i> Evaluación
                                                  </a>
                                             </li>
                                             <li class="nav-item">
                                                  <a class="nav-link" href="#prof" data-toggle="tab">
                                                       <i class="material-icons">person_pin</i> Facilitador
                                                  </a>
                                             </li>
                                             <li class="nav-item" >
                                                  <a class="nav-link" href="#mat" data-toggle="tab">
                                                       <i class="material-icons">personal_video</i> Materiales de apoyo
                                                  </a>
                                             </li>
                                        </ul>
                                   </div>
                              </div>
                         </div>
                         <div class="card-body ">
                              <div class="tab-content">
                                   <div class="tab-pane active show" id="prese">
                                        <br>
                                        <form-presentacion
                                             :material_introductorio="<?= htmlspecialchars(json_encode($material_introductorio), ENT_QUOTES, 'UTF-8'); ?>"
                                        ></form-presentacion>
                                   </div>
                                   <div class="tab-pane" id="modul">
                                        <lista-modulos
                                             :cursoid="<?= htmlspecialchars(json_encode($curso_id), ENT_QUOTES, 'UTF-8'); ?>"
                                        ></lista-modulos>
                                   </div>
                                   <div class="tab-pane" id="exam">
                                        <div class="col-md-12">
                                             <!-- Prexamen -->
                                             <examen
                                                  :cursoid="<?= htmlspecialchars(json_encode($curso_id), ENT_QUOTES, 'UTF-8'); ?>"
                                                  :type="<?= htmlspecialchars(json_encode(2), ENT_QUOTES, 'UTF-8'); ?>"
                                               ></examen>
                                             <br>
                                             <!-- Examen -->
                                             <examen
                                                  :cursoid="<?= htmlspecialchars(json_encode($curso_id), ENT_QUOTES, 'UTF-8'); ?>"
                                                  :type="<?= htmlspecialchars(json_encode(1), ENT_QUOTES, 'UTF-8'); ?>"
                                             ></examen>

                                        </div>

                                   </div>
                                   <div class="tab-pane" id="prof">
                                        <lista-profesores
                                             :cursoid="<?= htmlspecialchars(json_encode($curso_id), ENT_QUOTES, 'UTF-8'); ?>"
                                        ></lista-profesores>
                                   </div>
                                   <div class="tab-pane " id="mat">
                                        <lista-relacionados
                                             :cursoid="<?= htmlspecialchars(json_encode($curso_id), ENT_QUOTES, 'UTF-8'); ?>"
                                        ></lista-relacionados>
                                   </div>
                              </div>
                         </div>
                    </div>

               </div>
          </div>
     </div>
</div>

<fomulario-modulo
     :cursoid="<?= htmlspecialchars(json_encode($curso_id), ENT_QUOTES, 'UTF-8'); ?>"
></fomulario-modulo>
<fomulario-materiales
     :cursoid="<?= htmlspecialchars(json_encode($curso_id), ENT_QUOTES, 'UTF-8'); ?>"
></fomulario-materiales>
<fomulario-examen
     :cursoid="<?= htmlspecialchars(json_encode($curso_id), ENT_QUOTES, 'UTF-8'); ?>"
></fomulario-examen>
<formulario-profesores
     :cursoid="<?= htmlspecialchars(json_encode($curso_id), ENT_QUOTES, 'UTF-8'); ?>"
></formulario-profesores>
<formulario-material-relacionados
     :cursoid="<?= htmlspecialchars(json_encode($curso_id), ENT_QUOTES, 'UTF-8'); ?>"
></formulario-material-relacionados>

<fomulario-post-modulo></fomulario-post-modulo>
<fomulario-preguntas></fomulario-preguntas>
<preguntas-examen></preguntas-examen>
<fomulario-preguntas-examen :catalogo_preguntas = "<?= htmlspecialchars(json_encode($catalogo_preguntas), ENT_QUOTES, 'UTF-8'); ?>"></fomulario-preguntas-examen>

<!-- Mdal Upload -->
<modal-upload-presentacion></modal-upload-presentacion>

<!-- Mdal Upload Material-->
<modal-upload-material></modal-upload-material>

<!-- Mdal Upload Profesores-->
<modal-upload-profesores></modal-upload-profesores>

<!-- Mdal Upload Profesores-->
<modal-upload-relacionados></modal-upload-relacionados>
