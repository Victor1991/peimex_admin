<div class="content">

     <div class="container-fluid">
          <div class="row">
               <div class="col-md-12">
                    <div class="card-header card-header-primary" style="background-color: #2bbcd4; color: #fff; border-radius: 5px;">
                         <h4 class="card-title" style="color:#fff;"><?=$curso['nombre']?></h4>
                         <p class="card-category"><?=$curso['descripcion']?><br>
                              <a href="<?=base_url('cursos')?>" class="btn btn-default">Regresar</a>
                         </p>
                    </div>
               </div>
          </div>
          <br>
          <div class="row">
               <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="card card-stats">
                         <div class="card-header card-header-warning card-header-icon">
                              <div class="card-icon">
                                   <i class="material-icons">person</i>
                              </div>
                              <p class="card-category">Participante inscritos</p>
                              <h3 class="card-title"><?=$inscritos?>
                                   <small>Registrado(s)</small>
                              </h3>
                         </div>

                    </div>
               </div>
               
               <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="card card-stats">
                         <div class="card-header card-header-success card-header-icon">
                              <div class="card-icon">
                                   <i class="material-icons">how_to_reg</i>
                              </div>
                              <p class="card-category">Participante aprobados</p>
                              <h3 class="card-title"><?=$aprobados?>
                                   <small>Aprobados(s)</small>
                              </h3>
                         </div>

                    </div>
               </div>
               <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="card card-stats">
                         <div class="card-header card-header-danger card-header-icon">
                              <div class="card-icon">
                                   <i class="material-icons">highlight_off</i>
                              </div>
                              <p class="card-category">Participante reprobados</p>
                              <h3 class="card-title"><?=$reprobados?>
                                   <small>Reprobados(s)</small>
                              </h3>
                         </div>

                    </div>
               </div>
          </div>
          <div class="row">
               <div class="col-md-12 social-buttons-demo text-center">
                    <a href="<?=base_url('alumnos/exportar/'.$id)?>" class="btn btn-social btn-fill btn-success">
                         <i class="fa fa-file-excel-o" aria-hidden="true"  ></i> <strong> Descargar reporte</strong>
                    </a>
               </div>
          </div>
     </div>
</div>
