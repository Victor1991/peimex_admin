<div class="row" >
     <div class="col-md-12">
          <div class="card">
               <div class="card-header card-header-info">
                    <h4 class="card-title ">
                         Nuevo Administrador
                    </h4>
               </div>
               <div class="card-body">
                    <div class="col-md-12">
                         <?php show_alerts(); ?>
                         <form method="post" action="" class="form-horizontal" role="form">
                              <div class="row">
                                   <div class="col-md-6">
                                        <div class="form-group">
                                             <label class="bmd-label-floating">Roles</label>
                                             <?php echo form_dropdown('rol_id', $roles_dpdwn, '', 'class="form-control mtp" id="rol_id"  '); ?>
                                        </div>
                                   </div>
                                   <div class="col-md-6">
                                        <div class="form-group">
                                             <label class="bmd-label-floating">Activo</label>
                                             <br>
                                             <input
                                             type="checkbox"
                                             name="activo"
                                             id="estatus"
                                             value="1"
                                             onchange="_estatus()"
                                             />&nbsp;
                                             <h4 style="display: inline;" id="label_activo">Activo</h4>
                                        </div>
                                   </div>
                              </div>
                              <div class="row">
                                   <div class="col-md-6">
                                        <div class="form-group">
                                             <label class="bmd-label-floating">Nombre</label>
                                             <input type="text" class="form-control" name="nombre" id="nombre" value="<?php echo set_value('nombre'); ?>">
                                        </div>
                                   </div>
                                   <div class="col-md-6">
                                        <div class="form-group">
                                             <label class="bmd-label-floating">Apellidos</label>
                                             <input type="text" class="form-control" name="apellidos" id="apellidos" value="<?php echo set_value('apellidos'); ?>" >
                                        </div>
                                   </div>
                              </div>

                              <div class="row">
                                   <div class="col-md-6">
                                        <div class="form-group">
                                             <label class="bmd-label-floating">Email</label>
                                             <input type="email" class="form-control" name="email" id="email" value="<?php echo set_value('email'); ?>">
                                        </div>
                                   </div>
                                   <div class="col-md-6" id="nombre_marca">
                                        <div class="form-group">
                                             <label class="bmd-label-floating" id="lab_usr">Empresa</label>
                                             <input type="text" class="form-control" name="username" id="username" value="<?php echo set_value('username'); ?>" >
                                        </div>
                                   </div>

                                   <div class="col-md-6" id="marca_asignada">
                                        <div class="form-group">
                                             <label class="bmd-label-floating" id="lab_usr">Empresa asignada</label>
                                             <select class="form-control mtp" name="empresa_id" id="empresa_id">
                                                  <?php foreach ($clientes as $key => $cliente): ?>
                                                       <option
                                                       value="<?=$cliente->id?>"
                                                       <?php if(set_value('empresa_id') == $cliente->id ): ?>
                                                            selected
                                                       <?php endif; ?>
                                                       ><?=$cliente->username?></option>
                                                  <?php endforeach; ?>
                                             </select>
                                        </div>
                                   </div>


                                   <div class="col-md-6">
                                        <div class="form-group">
                                             <label class="bmd-label-floating">Nueva Contraseña</label>
                                             <input type="password" class="form-control" name="password" id="password" >
                                        </div>
                                   </div>
                                   <div class="col-md-6">
                                        <div class="form-group">
                                             <label class="bmd-label-floating">Confirmar contraseña</label>
                                             <input type="password" class="form-control" name="conf_password" id="conf_password" >
                                        </div>
                                   </div>
                              </div>

                              <div class="row">
                                   <div class="col-md-12">
                                        <a href="<?php echo base_url('usuarios'); ?>" class="btn btn-default">Cancelar</a>
                                        <?php if($this->acl->permiso('edita_usuarios')): ?>
                                             <button type="submit" class="btn btn-primary">Guardar</button>
                                        <?php endif; ?>
                                   </div>
                              </div>
                         </form>
                    </div>
               </div>
          </div>
     </div>
</div>
