<div class="row" >
     <div class="col-md-12">
          <div class="card">
               <div class="card-header card-header-info">
                    <h4 class="card-title ">
                         <h5>Listado de roles</h5>
                    </h4>
               </div>
               <div class="card-body">
                    <div class="col-md-12">
                         <?php show_alerts(); ?>
                         <br>
                         <form-roles
                                 :prop_rol='<?=  htmlspecialchars(json_encode($rol), ENT_QUOTES, 'UTF-8'); ?>'
                                 :prop_permisos_rol='<?=  htmlspecialchars(json_encode($permisos_rol), ENT_QUOTES, 'UTF-8'); ?>'
                                 :prop_permisos='<?=  htmlspecialchars(json_encode($permisos), ENT_QUOTES, 'UTF-8'); ?>'>
                         </form-roles>
                    </div>
               </div>
          </div>
     </div>
</div>

<?php _dump($permisos); ?>
