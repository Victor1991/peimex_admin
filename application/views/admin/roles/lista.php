<div class="row">
     <div class="col-md-12">
          <div class="card">
               <div class="card-header card-header-info">
                    <h4 class="card-title ">
                         Tabla roles
                         <a class="btn btn-sm btn-warning pull-right"> <i class="fa fa-plus"></i> </a>
                    </h4>
               </div>
               <div class="card-body">
                    <div class="box-alerts">
                         <?php show_alerts(); ?>
                    </div>
                    <tabla-roles :prop_roles='<?= htmlspecialchars(json_encode($roles), ENT_QUOTES, 'UTF-8'); ?>'></tabla-roles>
               </div>
          </div>
     </div>
</div>
