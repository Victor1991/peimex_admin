<div class="row">
     <div class="col-md-12">
          <div class="card">
               <div class="card-header card-header-info">
                    <h4 class="card-title ">
                         Importación de participantes
                         <a href="<?= base_url('/alumnos') ?>" class="btn btn-default btn-sm pull-right">
                              <i class="fa fa-chevron-left"></i>&nbsp;Atrás
                         </a>
                    </h4>
               </div>
               <div class="card-body">
                    <div class="box-alerts">
                         <?php show_alerts(); ?>
                    </div>
                    <div class="row">
                         <div class="col-md-12">
                              <div class="alert alert-primary">Para importar alumnos a continuación cargue su archivo csv,
                                   recuerde que el archivo debe seguir el formato correspondiente para no generar errores. Puede descargar un archivo de ejemplo desde el siguiente link
                                   <a href="<?=  base_url('files/ejemplo.csv')?>" style="font-weight: 700;" target="_blank">CSV Ejemplo</a></div>
                                   <form enctype="multipart/form-data" class="formulario">
                                        <?php if ($this->rol_id_user == 1) :?>
                                             <div class="col-md-12">
                                                  <label>Cliente</label>
                                                  <select class="form-control" name="cliente_id" id="cliente_id" onchange="get_cursos_cliente()">
                                                       <option value="" selected disabled hidden>
                                                            --- Cliente ---
                                                       </option>
                                                       <?php foreach ($clientes as $key => $cliente): ?>
                                                            <option value="<?=$cliente->id?>"><?=$cliente->username?></option>
                                                       <?php endforeach; ?>
                                                  </select>
                                             </div>
                                        <?php endif;?>
                                        <br>
                                        <div class="col-md-12">
                                             <label>Inscripciones al cursos</label>
                                             <select class="form-control" name="curso" id="curso" onchange="ver_form()">
                                                  <option value="" selected disabled hidden>
                                                       --- Curso ---
                                                  </option>
                                                  <?php if ($cursos['data']): ?>
                                                       <?php foreach ($cursos['data'] as $key => $curso): ?>
                                                            <option value="<?=$curso->id?>"><?=$curso->nombre?></option>
                                                       <?php endforeach; ?>
                                                  <?php endif; ?>

                                             </select>
                                        </div>
                                        <hr>
                                        <div class="col-md-12" id="form_upload" style="display:none;">
                                             <label>Subir un archivo</label><br />
                                             <input name="archivo" type="file" id="archivo" /><br/>
                                             <div class="messages"></div><br />
                                             <button type="button" class="btn btn-primary" id="btn-process">Importar datos</button><br />
                                        </div>

                                   </form>
                              </div>
                         </div>
                         <div class="row">
                              <div class="col-md-12">
                                   <!--div para visualizar en el caso de imagen-->
                                   <div class="showProcess">

                                   </div>
                                   <div class="loadProcess" style="display: none;">
                                        <img src="<?=base_url()?>/assets/admin/images/load.gif" style="width: 24px; margin-top: 12px;">
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>
