<div class="row">
     <div class="col-md-12">
          <div class="card">
               <div class="card-header card-header-info">
                    <h4 class="card-title ">
                         Tabla de participantes
                         <a href="<?=base_url('alumnos/form')?>" class="btn btn-sm btn-warning pull-right"> <i class="fa fa-plus"></i> </a>
                         <a href="<?=base_url('alumnos/importar')?>" class="btn btn-sm btn-warning pull-right"> <i class="fa fa-cloud-upload" aria-hidden="true"></i>  Importar </a>

                    </h4>
               </div>
               <div class="card-body">
                    <div class="col-md-12">
                         <filtros-tabla-alumnos :filtro="<?= htmlspecialchars(json_encode($filtos), ENT_QUOTES, 'UTF-8'); ?>" :prop_catalogos='<?= htmlspecialchars(json_encode($catalogos), ENT_QUOTES, 'UTF-8'); ?>'></filtros-tabla-alumnos>
                         <hr>
                         <tabla-alumnos></tabla-alumnos>
                    </div>
               </div>
          </div>
     </div>
</div>
