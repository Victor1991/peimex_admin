<div class="card card-nav-tabs">
     <div class="card-header card-header-info">
          <!-- colors: "header-primary", "header-info", "header-success", "header-warning", "header-danger" -->
          <div class="nav-tabs-navigation">
               <div class="nav-tabs-wrapper">
                    <ul class="nav nav-tabs" data-tabs="tabs">
                         <li class="nav-item">
                              <a class="nav-link active show" href="#tab_profile" data-toggle="tab">
                                   <i class="material-icons">school</i> <?=$form['id'] ? 'Editar participante ( '. $form['nombre'].' '. $form['apellido_paterno'].' '. $form['apellidos_materno'].' )' : 'Nuevo participante' ;?>
                              </a>
                         </li>
                         <?php if ($form['id'] ): ?>
                              <li class="nav-item">
                                   <a class="nav-link " href="#tab_cursos" data-toggle="tab">
                                        <i class="material-icons">local_library</i> Cursos inscrito
                                   </a>
                              </li>
                         <?php endif; ?>

                    </ul>
               </div>
          </div>
     </div>
     <div class="card-body ">
          <div class="tab-content text-center">
               <div class="tab-pane active show" id="tab_profile">
                    <br>
                    <formulario-alumnos
                         :informacion="<?= htmlspecialchars(json_encode($form), ENT_QUOTES, 'UTF-8'); ?>"
                         :clientes="<?= htmlspecialchars(json_encode($clientes), ENT_QUOTES, 'UTF-8'); ?>"
                    ></formulario-alumnos>
               </div>
               <div class="tab-pane"  id="tab_cursos">
                    <formulario-cursos-inscritos
                         :en_curso="<?= htmlspecialchars(json_encode($inscrito), ENT_QUOTES, 'UTF-8'); ?>"
                         :alumno_id="<?= htmlspecialchars(json_encode($form['id']), ENT_QUOTES, 'UTF-8'); ?>">
                    </formulario-cursos-inscritos>

               </div>

          </div>
     </div>
</div>

<modal-input :usuario_id=<?=htmlspecialchars(json_encode($form['id']), ENT_QUOTES, 'UTF-8')?>></modal-input>
