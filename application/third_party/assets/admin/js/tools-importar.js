$(document).ready(function(){

    $(".messages").hide();
    //queremos que esta variable sea global
    var fileExtension = "";
    //función que observa los cambios del campo file y obtiene información
    $(':file').change(function() {
        var file = $("#archivo")[0].files[0];
        //obtenemos el nombre del archivo
        var fileName = file.name;
        //obtenemos la extensión del archivo
        fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
        //obtenemos el tamaño del archivo
        var fileSize = file.size;
        //obtenemos el tipo de archivo image/png ejemplo
        var fileType = file.type;
        //mensaje con la información del archivo
        if (isCsv(fileExtension)) {
            showMessage("<span class='text-success'>Archivo para subir: "+fileName+", peso total: "+bytesToSize(fileSize));
            $('#btn-process').attr('disabled', false);
        } else {
            showMessage("<span class='text-danger'>El tipo de archivo no es correcto, solo se permiten archivos \".csv\": "+fileName+", peso total: "+bytesToSize(fileSize));
            $('#btn-process').attr('disabled', true);
        }
    });

    //al enviar el formulario
    $('#btn-process').click(function(){
        $(".showProcess").html("");
        var formData = new FormData($(".formulario")[0]);
        var message = "";
        //hacemos la petición ajax
        $.ajax({
            url: base_url + '/alumnos/cargar_archivo',
            type: 'POST',
            // Form data
            //datos del formulario
            data: formData,
            //necesario para subir archivos via ajax
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            //mientras enviamos el archivo
            beforeSend: function(){
                $('#btn-process').attr('disabled', true);
                $('.loadProcess').show();
                message = "<span>Subiendo el archivo, por favor espere...</span><br>";
                showProcessMessage(message);
            },
            success: function(data){
                if (data.status == 'ok') {
                    message = "<span>El archivo se ha subido correctamente.</span><br>";
                    showProcessMessage(message);
                    importar(data.file);
                } else {
                    message = "<span class='text-danger'>" + data.message + "</span><br>";
                    showProcessMessage(message);
                    $('.loadProcess').hide();
                }
            },
            error: function(){
                $('.loadProcess').hide();
                message = "<span class='text-danger'>Ha ocurrido un error.</span><br>";
                showProcessMessage(message);
            }
        });
    });
});

function importar(file) {
    var message = '';
    $.ajax({
        url: base_url + '/alumnos/procesar_archivo',
        type: 'POST',
        data: { archivo: file, curso_id : $('#curso').val() },
        dataType: 'json',
        //mientras enviamos el archivo
        beforeSend: function() {
            message = "<span>Procesando el archivo...</span><br>";
            showProcessMessage(message);
        },
        success: function(data) {
            if (data.status == 'ok') {
                message = "<span class='text-success'>Su archivo fue importado</span><br>"
                    + "<span>"+data.data.total+" registros procesados.</span><br>"
                    + "<span>"+data.data.updated+" registros actualizados.</span><br>"
                    + "<span>"+data.data.news+" registros nuevos.</span><br>"
                    + "<span class='text-danger'>"+data.data.errors+" registros contienen errores.</span><br>";
                showProcessMessage(message);
            } else {
                message = "<span class='text-danger'>"+data.message+"</span><br>";
                showProcessMessage(message);
            }
            $('.loadProcess').hide();
        },
        error: function(){
            $('.loadProcess').hide();
            message = "<span class='text-danger'>Ha ocurrido un error al procesar el archivo.</span><br>";
            showProcessMessage(message);
        }
    });
}

//como la utilizamos demasiadas veces, creamos una función para
//evitar repetición de código
function showMessage(message){
    $(".messages").html("").show();
    $(".messages").html(message);
}

function showProcessMessage(message){
    $(".showProcess").append(message);
}

function isCsv(extension) {
    if (extension.toLowerCase() == 'csv') {
        return true;
    } else {
        return false;
    }
}

//comprobamos si el archivo a subir es una imagen
//para visualizarla una vez haya subido
function isImage(extension) {
    switch(extension.toLowerCase())
    {
        case 'jpg': case 'gif': case 'png': case 'jpeg':
            return true;
        break;
        default:
            return false;
        break;
    }
}

function ver_form() {
     if($('#curso').val() != ''){
          $('#form_upload').show();
     }
}

function bytesToSize(bytes) {
   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
   if (bytes == 0) return '0 Byte';
   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}
