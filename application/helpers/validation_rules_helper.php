<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('test_method'))
{
    function get_reglas_facturacion(){
        return [
            'razon_social' => [
                'field' => 'facturacion[razon_social]',
                'label' => 'Razón social',
                'rules' => 'required'
            ],
            'email_factura' => [
                'field' => 'facturacion[email_factura]',
                'label' => 'Correo electrónico para envío de factura',
                'rules' => 'required|valid_email'
            ],
            'uso_cfdi' => [
                'field' => 'facturacion[uso_cfdi]',
                'label' => 'Uso de CFDI',
                'rules' => 'required'
            ],
            'forma_pago' => [
                'field' => 'facturacion[forma_pago]',
                'label' => 'Forma de pago',
                'rules' => 'required'
            ],
            'metodo_pago' => [
                'field' => 'facturacion[metodo_pago]',
                'label' => 'Método de pago',
                'rules' => 'required'
            ],
            'calle' => [
                'field' => 'direccion[calle]',
                'label' => 'Calle',
                'rules' => 'required'
            ],
            'num_ext' => [
                'field' => 'direccion[num_ext]',
                'label' => 'Núm. Exterior',
                'rules' => 'required'
            ],
            'cp' => [
                'field' => 'direccion[cp]',
                'label' => 'C.P.',
                'rules' => 'required'
            ],
            'colonia' => [
                'field' => 'direccion[colonia]',
                'label' => 'Colonia',
                'rules' => 'required'
            ],
            'municipio' => [
                'field' => 'direccion[municipio]',
                'label' => 'Municipio',
                'rules' => 'required'
            ],
            'estado' => [
                'field' => 'direccion[estado]',
                'label' => 'Estado',
                'rules' => 'required'
            ]
        ];
    }

}

