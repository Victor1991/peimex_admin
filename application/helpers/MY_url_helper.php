<?php

function theme_assets($uri) {
    $CI = & get_instance();
    return $CI->config->base_url('appcms/themes/' . FRONT_THEME . '/assets/' . $uri);
}

function admin_assets($uri) { 
    $CI = & get_instance();
    return $CI->config->base_url('assets/admin/' . $uri);
}