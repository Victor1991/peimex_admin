<?php
class Profesores extends Admin_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model(array('profesores_model'));
		$this->load->library('form_validation');
	}

     public function get_profesor(){
          $curso_id = $this->input->post('cursoId');
		$array = $this->profesores_model->get_profesor($curso_id);
          echo json_encode($array);
     }

	public function eliminar_profesor(){
          $profesor_id = $this->input->post('profesor_id');
          $mensaje  = array('mensaje' => 'Ocurrio un error', 'error' => true);
          if ($this->profesores_model->delete_profesor($profesor_id)) {
               $mensaje  = array('mensaje' => 'Exito al eliminar al profesor', 'error' => false);
          }
          echo json_encode($mensaje);
     }

	public function get_profesor_id(){
          $profesor_id = $this->input->post('profesor_id');
		$data = array('curso_id' => '' ,
					'descripcion' => '' ,
					'id' => '' ,
					'porcentaje_aprobar' => '' ,
					'titulo' => ''
				);
		$data = array_merge($data, (array)$this->profesores_model->get_profesor_id($profesor_id));
          echo json_encode($data);
     }

	public function data_form(){
		$this->form_validation->set_rules('nombre', 'Nombre', 'required');
          if ($this->form_validation->run()) {
               $profesor['nombre'] = $this->input->post('nombre');
               $profesor['descripcion'] = $this->input->post('descripcion');
			$profesor['curso_id'] = $this->input->post('curso_id');
               $profesor['foto'] = $this->input->post('foto');
               if ( $this->input->post('id')) {
                    $this->profesores_model->editar_profesor($this->input->post('id'), $profesor);
                    $this->session->set_flashdata('mensajes', 'Se ha actualizado el curso ');
                    $res = [
                         'status' => 'ok',
                    ];
               }else{
                    $this->profesores_model->new_profesor($profesor);
                    $this->session->set_flashdata('mensajes', 'Se ha creado el curso ');
                    $res = [
                         'status' => 'ok',
                    ];
               }

               echo json_encode($res);
          } else {
               $res = [
                    'status' => 'error',
                    'errores' => validation_errors()
               ];
               echo json_encode($res);
          }
	}
}
