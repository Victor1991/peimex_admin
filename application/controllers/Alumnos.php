<?php

require './vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class Alumnos extends Admin_Controller {
	private $import_data;

	private $validation_rules = array(
		'nombre' => array(
			'field' => 'nombre',
			'label' => 'Nombre',
			'rules' => 'required'
		),
		'apellido_paterno' => array(
			'field' => 'apellido_paterno',
			'label' => 'Apellidos paterno',
			'rules' => 'required'
		),
		'apellidos_materno' => array(
			'field' => 'apellidos_materno',
			'label' => 'Apellidos materno',
			'rules' => 'required'
		),
		'correo' => array(
			'field' => 'correo',
			'label' => 'Correo',
			'rules' => 'required|valid_email'
		),
	);

	public function __construct() {
		parent::__construct();
		$this->modulo = 5;
		$this->nombre_modulo = 'Participante';
		$this->load->model(	array('cursos_model','administrador_model', 'cursos_model', 'alumnos_model', 'institucion_model', 'administrador_model', 'examen_model', 'preguntas_model') );
		$this->import_data = [
			'total' => 0,
			'updated' => 0,
			'news' => 0,
			'errors' => 0,
			'rows_error' => []
		];
		$this->load->library('form_validation');

	}

	public function index(){
		$data['catalogos'] = [
				'roles' => $this->rol_model->get_opciones_dropdown(),
				'empresas' => $this->administrador_model->get_all( $params = ['rol_id' => 2])
		];
		$data['filtos'] =   array('activo' => '' );

		$this->view('alumnos/lista',$data);
	}

	public function eliminar_alumno($alumno_id){
		$mensaje  = array('mensaje' => 'Ocurrio un error', 'error' => true);
		if ($this->alumnos_model->delete($alumno_id)) {
			$mensaje  = array('mensaje' => 'Exito al eliminar al alumno', 'error' => false);
		}
		echo json_encode($mensaje);

	}

	public function form($id = false){
		$this->add_asset('css', 'css/plugins/switchery/switchery.css');
		$this->add_asset('js', 'js/plugins/switchery/switchery.js');
		$this->add_asset('js', 'js/usuarios.js');

		$data = array(
			'id' => '',
			'nombre' => '',
			'estatus' => '',
			'apellido_paterno' => '',
			'apellidos_materno' => '',
			'correo' => '',
			'celular' => '',
			'curp' => '',
			'profesion' => '',
			'sucursal' => '',
			'departamento' => '',
			'puesto' => '',
			'txt_pas' => '',
			'empresa' => '',
			'cliente_id' => '',
			'inputs' => array(),
			'cliente_id' => ''
		);

		$form['inscrito'] = array();
		if ($id) {
			$data = array_merge($data, $this->alumnos_model->get_by_id($id, true));
			$form['inscrito'] = $this->alumnos_model->alumnos_curso_inscrito($id);
			$data['inputs'] = $this->alumnos_model->get_input_alumno($id);
			$data['id'] = $id;
		}
		$data['administrador'] = $this->administrador_model->get_all(array('rol_id' => 2 ));
		unset($data['password']);
		$form['form'] = $data;
		$form['clientes'] = $this->administrador_model->get_all(['rol_id' => 2]);
		$this->view('alumnos/form', $form);
	}

	public function inscrito_en_curso(){
		$curso_id = $this->input->post('curso_id');
		$alumno_id = $this->input->post('alumno_id');
		echo json_encode($this->alumnos_model->validar_alumno_in_curso($alumno_id, $curso_id));
	}

	public function inscribir(){
		$curso_id = $this->input->post('curso_id');
		$alumno_id = $this->input->post('alumno_id');
		$inscrito = $this->alumnos_model->validar_alumno_in_curso($alumno_id, $curso_id);
		if ($inscrito) {
			$this->alumnos_model->eliminar_alumnos_curso_inscrito($alumno_id, $curso_id);
			$mensaje['mensaje'] =  'Baja del curso';
		}else{
			$save['usuario_id'] = $alumno_id;
			$save['curso_id'] = $curso_id;
			$curso = $this->cursos_model->get_curso_id($curso_id)['nombre'];
			$usuario = $this->alumnos_model->get_by_id($alumno_id);
			$this->mail($usuario, $curso, false);
			$this->alumnos_model->inser_alumnos_curso_inscrito($save);
			$mensaje['mensaje'] =  'Inscrito al curso';

		}
		$mensaje['errores'] =  '';
		echo json_encode($mensaje);
	}

	public function get_alumnos(){
		$data['usuario']= array(
			'activo' => isset($_POST['activo']) ? $this->input->post('activo') : '' ,
			'buscar' => isset($_POST['buscar']) ? $this->input->post('buscar') : '',
			'cliente_id' => isset($_POST['empresa']) ? $this->input->post('empresa') : '',
			'range' => isset($_POST['range']) ? $this->input->post('range') : array( 20, 0)
		);


		echo json_encode($this->alumnos_model->get_alumnos($_POST), true);

	}


	public function insertar() {
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->validation_rules['correo']['rules'] .= '|callback__email_check';
		if ($this->input->post('id') == '') {
			$this->form_validation->set_rules('correo', 'Correo', 'is_unique[usuarios.correo]');
			$this->form_validation->set_rules('password', 'Password', 'required');
			$this->form_validation->set_rules('confirm_password', 'Confirmar Password', 'required|matches[password]');
		}else{
			$this->form_validation->set_rules('correo', 'Correo', 'is_unique[usuarios.correo, id, {id}]');
			if ($this->input->post('password') || $this->input->post('confirm_password')) {
				$this->form_validation->set_rules('password', 'Password', 'required');
				$this->form_validation->set_rules('confirm_password', 'Confirmar Password', 'required|matches[password]');
			}
		}

		if ($this->rol_id_user == 1) {
			$this->form_validation->set_rules('cliente_id', 'Empresa', 'required');
		}

		$this->form_validation->set_rules($this->validation_rules);

		$mensaje = array( 'error' => true, 'mensaje' => '', 'errores' => '' );

		$this->breadcrumbs->push('Agregar', 'admin/usuarios/agregar');
		if ($this->form_validation->run() === FALSE) {
			$mensaje['errores'] =  $this->form_validation->error_array();
			$mensaje['mensaje'] =  'Se requieren campos';
			echo json_encode($mensaje);

		} else {
			$usuario['nombre'] = $this->input->post('nombre');
			$usuario['rol_id'] = '0';
			$usuario['estatus'] = $this->input->post('estatus') == 'true' ? 1 : 0;
			$usuario['apellido_paterno'] = $this->input->post('apellido_paterno');
			$usuario['apellidos_materno'] = $this->input->post('apellidos_materno');
			$usuario['correo'] = $this->input->post('correo');

			$alumno['celular'] = $this->input->post('celular');
			$alumno['curp'] = $this->input->post('curp');
			$alumno['profesion'] = $this->input->post('profesion');
			$alumno['sucursal'] = $this->input->post('sucursal');
			$alumno['departamento'] = $this->input->post('departamento');
			$alumno['puesto'] = $this->input->post('puesto');
			$alumno['empresa'] = $this->input->post('empresa');
			$alumno['cliente_id'] = $this->input->post('cliente_id');


			$old_info =  $this->alumnos_model->get_by_id($this->input->post('id'), true);


			if ($this->rol_id_user == 2) {
				$alumno['cliente_id'] =  $this->session->userdata('admin')['usuario_id'];
			}elseif ($this->rol_id_user == 3) {
				$alumno['cliente_id'] = $this->session->userdata('admin')['empresa_id'];
			}else{
				$alumno['cliente_id'] =  $this->input->post('cliente_id');
			}



			if ($this->input->post('password')) {
				$usuario['txt_pas'] = $this->input->post('password');
				$usuario['password'] = $this->auth->hash_password($this->input->post('password'));
			}

			if ($this->input->post('id')) {
				$usuario['fecha_actualizacion'] = date("Y-m-d H:i:s");
				$this->alumnos_model->update($this->input->post('id'), $usuario);
				$this->alumnos_model->eliminar_insertar_alumno($alumno, $this->input->post('id'));

				if ($this->alumnos_model->update_alumno($this->input->post('id'), $alumno)) {
					if($old_info['txt_pas'] != $this->input->post('password')){
						$this->mail_cambio_password($_POST);
					}
					$mensaje['error'] =  false;
					$mensaje['errores'] =  '';
					$mensaje['mensaje'] =  'Exito al editar el usuario';
					echo json_encode($mensaje);
				}else{
					$mensaje['errores'] =  '';
					$mensaje['mensaje'] =  'Error al insertar el usuario';
					echo json_encode($mensaje);
				}

			}else{
				$usuario['fecha_creacion'] = date("Y-m-d H:i:s");
				$usuario['fecha_actualizacion'] = $usuario['fecha_creacion'];
				$alumno['usuario_id'] = $this->alumnos_model->insert($usuario);
				if ($this->alumnos_model->insertar_alumno($alumno)) {
					$this->mail_usuario_manual($_POST);
					$mensaje['error'] =  false;
					$mensaje['errores'] =  '';
					$mensaje['mensaje'] =  'Exito al insertar el usuario';
					echo json_encode($mensaje);
				}else{
					$mensaje['errores'] =  '';
					$mensaje['mensaje'] =  'Error al insertar el usuario';
					echo json_encode($mensaje);
				}
			}
		}
	}

	public function _username_check() {
		if ($this->auth->existe_username($this->input->post('username'))) {
			$this->form_validation->set_message('_username_check', 'El username ya existe');
			return false;
		}
		return true;
	}

	public function _email_check() {
		if ($this->auth->existe_email($this->input->post('email'))) {
			$this->form_validation->set_message('_email_check', 'El email ya es usado por otra cuenta.');
			return false;
		}
		return true;
	}


	public function importar(){
		$this->add_asset('js', 'js/tools-importar.js');
		if ($this->rol_id_user == 2 || $this->rol_id_user == 3) {
			$data['cursos'] = $this->cursos_model->get_cursos($_POST);
		}else{
			$data['cursos'] = array();
		}
		$data['clientes'] = $this->administrador_model->get_all(['rol_id' => 2]);
		$this->view('alumnos/importar', $data);
	}

	public function cargar_archivo() {
		//comprobamos que sea una petición ajax
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

			//obtenemos el archivo a subir
			$file = md5(time()) . '.csv';
			//$file = $_FILES['archivo']['name'];

			$ext = pathinfo($_FILES['archivo']['name'], PATHINFO_EXTENSION);
			if (strtolower($ext) != 'csv') {
				echo json_encode([
					'status' => 'error',
					'message' => 'El tipo de archivo no es válido. Solo se permiten archivos csv.'
				]);
				return;
			}

			//comprobamos si existe un directorio para subir el archivo
			//si no es así, lo creamos
			if(!is_dir("files/"))
			mkdir("files/", 0777);

			//comprobamos si el archivo ha subido
			if ($file && move_uploaded_file($_FILES['archivo']['tmp_name'],"files/".$file)) {
				sleep(3);//retrasamos la petición 3 segundos
				//devolvemos el nombre del archivo para pintar la imagen
				echo json_encode([
					'status' => 'ok',
					'file' => $file
				]);
			} else {
				echo json_encode([
					'status' => 'error',
					'message' => 'No fue posible cargar el archivo, verifique que su archivo no este corrupto.'
				]);
			}
		}else{
			throw new Exception("Error Processing Request", 1);
		}
	}


	public function procesar_archivo() {
		if ($this->input->is_ajax_request()) {
			ini_set('memory_limit', '256M');
			$this->load->library('csvimport');
			$this->load->helper('file');

			$archivo = $this->input->post('archivo');
			// $archivo = '423a86f6b1f64ad11216237dfd4b101d.csv';
			$curso_id = $this->input->post('curso_id');
			$cliente_id = $this->input->post('cliente_id');
			$pathfile = "./files/" . $archivo;
			if (file_exists($pathfile)) {
				$columns_headers =
				[
					'nombre',
					'apellido_paterno',
					'apellidos_materno',
					'curp',
					'profesion',
					'sucursal',
					'departamento',
					'puesto',
					'correo',
					'celular',
					'empresa'
				];

				$items = $this->csvimport->get_array($pathfile, $columns_headers, TRUE, FALSE, FALSE, TRUE, TRUE);


				if($this->save_alumnos($items, $curso_id, $cliente_id)) {
					echo json_encode([
						'status' => 'ok',
						'data' => $this->import_data
					]);
				} else {
					echo json_encode([
						'status' => 'error',
						'message' => $this->error_message
					]);
				}

				@unlink($pathfile);
			}
		} else {
			show_404();
		}
	}


	private function save_alumnos($alumnos, $curso_id, $cliente_id = false) {

		if (is_array($alumnos) && count($alumnos) > 0) {
			$curso = $this->cursos_model->get_curso_id($curso_id)['nombre'];

			$this->import_data['total'] = count($alumnos) - 1;
			$first = TRUE;
			$en_lista = array();
			foreach ($alumnos as $value) {
				if ($first) {
					$first = FALSE;
					continue;
				}

				if (count($value) != 11) {
					$this->error_message = 'El número de columnas del archivo no es el correcto';
					return FALSE;
				}

				if (empty($value['correo'])) {
					//error en los datos
					$this->import_data['errors']++;
					$this->import_data['rows_error'][] = $value;
					continue;
				} else {
					$usuario = array();
					$alum = array();
					$usuario['nombre'] = $value['nombre'];
					$usuario['apellido_paterno'] = $value['apellido_paterno'];
					$usuario['apellidos_materno'] = $value['apellidos_materno'];
					$usuario['estatus'] = '1';
					$value['password'] = $this->generateRandomString();
					$pass = $value['password'];
					$usuario['txt_pas'] = $value['password'];
					$usuario['password'] = $this->auth->hash_password($value['password']);
					$usuario['fecha_creacion'] = date('Y-m-d H:i:s');
					$usuario['correo'] = $value['correo'];

					$alum['puesto'] = $value['puesto'];
					$alum['departamento'] = $value['departamento'];
					$alum['sucursal'] = $value['sucursal'];
					$alum['sucursal'] = $value['sucursal'];
					$alum['profesion'] = $value['profesion'];
					$alum['curp'] = $value['curp'];
					$alum['celular'] = $value['celular'];

					if ($this->rol_id_user == 1) {
						$alum['cliente_id'] = $cliente_id;
					}elseif ($this->rol_id_user == 2) {
							$alum['cliente_id'] = $this->session->userdata('admin')['usuario_id'];
					}else{
						$alum['cliente_id'] = $this->session->userdata('admin')['empresa_id'];
					}

					$item = (array)$this->alumnos_model->get_by_correo($value['correo']);
					if ($item) {
						unset($usuario['password']);
						$correo =  $usuario['correo'];
						$alum['usuario_id'] = $item['id'];
						unset($usuario['correo']);
						unset($usuario['txt_pas']);
						$this->alumnos_model->update($item['id'], $usuario);
						$this->alumnos_model->eliminar_insertar_alumno($alum ,$item['id']);
						// $dt = $this->alumnos_model->get_alumnos_by_user_id($item['id']);
						// if ($dt) {
						// 	$this->alumnos_model->update_alumno($item['id'], $alum);
						// }else {
						// 	$alum['usuario_id'] = $item['id'];
						// 	$this->alumnos_model->insertar_alumno($alum);
						// }
						if (!$this->alumnos_model->validar_alumno_in_curso($item['id'], $curso_id)) {
							$ins['usuario_id'] = $item['id'];
							$ins['curso_id'] =$curso_id;
							$this->alumnos_model->inser_alumnos_curso_inscrito($ins);
						}
						$usuario['correo'] = $correo ;
						$this->import_data['updated']++;
						$this->mail($usuario, $curso, false);
					} else {
						$usuario_id = $this->alumnos_model->insert($usuario);
						$alum['usuario_id'] = $usuario_id;
						$this->alumnos_model->insertar_alumno($alum);
						$ins['usuario_id'] = $usuario_id;
						$ins['curso_id'] =$curso_id;
						$this->alumnos_model->inser_alumnos_curso_inscrito($ins);
						$this->import_data['news']++;
						$usuario['password'] = $pass;
						$this->mail($usuario, $curso, true);
					}


				}
			}

			return TRUE;
		} else {
			$this->error_message = 'El archivo esta vacio o no es posible leer los datos del mismo, '
			. 'revise que el formato de los datos sea correcto.';

			return FALSE;
		}


	}

	public function generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	public function mail($data, $curso, $nuevo ){
		// load email library
		$this->load->library('email');
		// from address
		$this->email->from('no-reply@capapei.com', 'capapei.com');
		$this->email->to($data['correo']); // to Email address
		$this->email->subject('Inscripción'); // email Subject
		$mensaje = '';
		$mensaje .= 'Login página : http://capapei.com/sistema/<br>' ;
		$mensaje .= 'Curso inscrito : ' .$curso . '<br>'  ;
		if ($nuevo === false) {
			$mensaje .= 'Ingresa con tus credenciales ' ;
		}else{
			$mensaje .= 'Usuario : ' .$data['correo'] . '<br>'  ;
			$mensaje .= 'Password : '  .$data['txt_pas'] . '<br>'  ;
		}
		$mensaje .= '<br><hr><br> Recomendación: Usar navegador Google Chrome para el uso del sistema" "para cualquier problema o duda para navegar en el sistema manden un correo a: soporte@capapei.com"' ;

		$this->email->message($mensaje);
		$this->email->send(); // send Email
	}


	// Inputs
	public function get_input_alumno(){

	}

	public function get_input_id(){

	}

	public function mail_usuario_manual($data){
		$this->load->library('email');
		// from address
		$this->email->from('no-reply@capapei.com', 'capapei.com');
		$this->email->to($data['correo']); // to Email address
		$this->email->subject('Registrado a la plataforma'); // email Subject
		$mensaje = '';
		$mensaje .= 'Login página : http://capapei.com/sistema/<br>' ;
		$mensaje .= 'Usuario :'.$data['correo'].' <br>' ;
		$mensaje .= 'Contraseña :'.$data['password'].' <br>' ;
		$mensaje .= '<br><hr><br> Recomendación: Usar navegador Google Chrome para el uso del sistema" "para cualquier problema o duda para navegar en el sistema manden un correo a: soporte@capapei.com"' ;
		$this->email->message($mensaje);
		$this->email->send(); // send Email
	}


	public function mail_cambio_password($data){
		$this->load->library('email');
		// from address
		$this->email->from('no-reply@capapei.com', 'capapei.com');
		$this->email->to($data['correo']); // to Email address
		$this->email->subject('Cabio de contraseña'); // email Subject
		$mensaje = '';
		$mensaje .= 'Login página : http://capapei.com/sistema/<br>' ;
		$mensaje .= 'nueva contraseña :'.$data['password'].' <br>' ;
		$mensaje .= '<br><hr><br> Recomendación: Usar navegador Google Chrome para el uso del sistema" "para cualquier problema o duda para navegar en el sistema manden un correo a: soporte@capapei.com"' ;
		$this->email->message($mensaje);
		$this->email->send(); // send Email
	}

	public function data_input(){
		$this->form_validation->set_rules('titulo', 'Titulo', 'required');
		if ($this->form_validation->run()) {
			$input['alumno_id'] = $this->input->post('alumno_id');
			$input['titulo'] = $this->input->post('titulo');
			$input['valor'] = $this->input->post('valor');
			if ( $this->input->post('id')) {
				$this->alumnos_model->update_input($this->input->post('id'), $input);
				$this->session->set_flashdata('mensajes', 'Se ha actualizado la prensentación ');
				$res = [
					'status' => 'ok',
				];
			}else{
				$input['creado'] = date('Y-m-d');
				$this->alumnos_model->insert_input($input);
				$this->session->set_flashdata('mensajes', 'Se ha creado la presentación ');
				$res = [
					'status' => 'ok',
				];
			}

			echo json_encode($res);
		} else {
			$res = [
				'status' => 'error',
				'errores' => validation_errors()
			];
			echo json_encode($res);
		}
	}

	public function delete_input($input_id){
		$mensaje  = array('mensaje' => 'Ocurrio un error', 'error' => true);
          if ($this->alumnos_model->delete_input($input_id)) {
               $mensaje  = array('mensaje' => 'Exito al eliminar al input', 'error' => false);
          }
          echo json_encode($mensaje);
	}



     public function exportar($curso_id) {
		$curso = $this->cursos_model->get_curso_id($curso_id);
          $filename =  $curso['nombre'].' ' . date('d-m-y_H-m') . '.xlsx';
          $spreadsheet = new Spreadsheet();
          $sheet = $spreadsheet->getActiveSheet();


		$letras = array('K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ');

		$alumnos = $this->alumnos_model->get_alumnos_curso($curso_id);

		// _dump($this->db->last_query());
		// die;

		$examen = $this->examen_model->get_examen($curso_id,1);
		if ($examen) {
			$preg_examen = $this->preguntas_model->get_preguntas_examen($examen->id);
			foreach ($alumnos as $key => $alumno) {
				$alumnos[$key]->examen = $this->get_respustas_examen($preg_examen, $alumno->intento_id);

			}
		}

          //headers
		$sheet->setCellValue('A1', 'Nombre')->getStyle('A1')->getFont()->setBold(true);
          $sheet->setCellValue('B1', 'Apellido Paterno')->getStyle('B1')->getFont()->setBold(true);
          $sheet->setCellValue('C1', 'Apellido Materno')->getStyle('C1')->getFont()->setBold(true);
          $sheet->setCellValue('D1', 'Correo')->getStyle('D1')->getFont()->setBold(true);
          $sheet->setCellValue('E1', 'Num. Intento')->getStyle('E1')->getFont()->setBold(true);
          $sheet->setCellValue('F1', 'Fecha Inicio')->getStyle('F1')->getFont()->setBold(true);
          $sheet->setCellValue('G1', 'Fecha fin')->getStyle('G1')->getFont()->setBold(true);
          $sheet->setCellValue('H1', 'Calificacion')->getStyle('H1')->getFont()->setBold(true);
          $sheet->setCellValue('I1', 'Cliente')->getStyle('I1')->getFont()->setBold(true);
		$sheet->setCellValue('J1', 'Curso')->getStyle('J1')->getFont()->setBold(true);
		$sheet->setCellValue('K1', 'Correctas')->getStyle('K1')->getFont()->setBold(true);
		$sheet->setCellValue('L1', 'Incorrectas')->getStyle('L1')->getFont()->setBold(true);
		if (isset($alumnos[0]->examen[0])) {
			if (count($alumnos[0]->examen[0]) > 0 ) {
				$pregunta = 1;
				foreach ($alumnos[0]->examen[0] as $key => $preguntas) {
					$sheet->setCellValue($letras[$key].'1', 'Pregunta '.$pregunta.' -> '.$preguntas)->getStyle($letras[$key].'1')->getFont()->setBold(true);
					\PhpOffice\PhpSpreadsheet\Cell\Cell::setValueBinder( new \PhpOffice\PhpSpreadsheet\Cell\AdvancedValueBinder() );

					$spreadsheet->getActiveSheet()->getStyle($letras[$key].'1')->getAlignment()->setWrapText(true);
					$spreadsheet->getActiveSheet()->getStyle($letras[$key].'1')->getAlignment()->setWrapText(true);
					$pregunta++;
				}
			}
		}

		for ($i = 0; $i < count($alumnos); $i++) {
			$sheet->setCellValue('A' . ($i+2), $alumnos[$i]->nombre);
			$sheet->setCellValue('B' . ($i+2), $alumnos[$i]->apellido_paterno);
			$sheet->setCellValue('C' . ($i+2), $alumnos[$i]->apellidos_materno);
			$sheet->setCellValue('D' . ($i+2), $alumnos[$i]->correo);
			$sheet->setCellValue('E' . ($i+2), $alumnos[$i]->num_intento);
			$sheet->setCellValue('F' . ($i+2), !is_null($alumnos[$i]->inicio) ? date("d-m-Y H:i:s", strtotime($alumnos[$i]->inicio)) : 'S/F' ) ;
			$sheet->setCellValue('G' . ($i+2), !is_null($alumnos[$i]->fin) ? date("d-m-Y H:i:s", strtotime($alumnos[$i]->fin)) : 'S/F' ) ;
			$sheet->setCellValue('H' . ($i+2), $alumnos[$i]->calificacion);
			$sheet->setCellValue('I' . ($i+2), $alumnos[$i]->cliente);
			$sheet->setCellValue('J' . ($i+2), $alumnos[$i]->curso);
			if (isset($alumnos[0]->examen[0])) {
				if (count($alumnos[$i]->examen[1]) > 0 ) {
					foreach ($alumnos[$i]->examen[1] as $key => $repuestas) {
						$cn = $i+2;
						$sheet->setCellValue($letras[$key].$cn, $repuestas);
					}
				}
			}

		}

		$spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$spreadsheet->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		$spreadsheet->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
		if (isset($alumnos[0]->examen[0])) {
			if (count($alumnos[0]->examen[0]) > 0 ) {
				foreach ($alumnos[0]->examen[0] as $key => $preguntas) {
					$spreadsheet->getActiveSheet()->getColumnDimension($letras[$key])->setAutoSize(true);
				}
			}
		}



          $writer = new Xlsx($spreadsheet);
          header('Content-Type: application/vnd.ms-excel');
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

          header('Content-Disposition: attachment;filename="'.$filename.'"'); /*-- $filename is  xsl filename ---*/
          header('Cache-Control: max-age=0');

          //ob_end_clean();

          $writer->save('php://output');
     }

	public function get_respustas_examen($preguntas, $intento){
		// $correctas = array();
		// $repuestas_list = array();
		$repuestas_list = array();
		$preguntas_lita = array();
		foreach ($preguntas as $key => $pregunta) {
			switch ($pregunta->catalogo_preguntas) {
                    case 1:
                    case 4:
                    case 5:
                    case 6:
                    case 0:
					$preguntas_lita[] = $pregunta->titulo;
					$repuesta = $this->preguntas_model->get_respuesta_alumno($pregunta->id, $intento);
					if ($repuesta) {
						$key = array_search($repuesta->respuesta_id, array_column($pregunta->respuestas, 'id'));

						if( $pregunta->respuestas[$key]->correcta == 1 ){
							array_push($repuestas_list, 'correcta');
						}else{
							array_push($repuestas_list, 'incorrecta');
						};
					}
                    break;
                    case '2':
					foreach($pregunta->pregunta_hijo as $key => $_pregunta) {
						$preguntas_lita[] = $_pregunta->titulo;
						$repuesta = $this->preguntas_model->get_respuesta_alumno($_pregunta->id, $intento);
						if ($repuesta) {
							$key = array_search($repuesta->respuesta_id, array_column($_pregunta->respuestas, 'id'));
							if( $_pregunta->respuestas[$key]->correcta == 1 ){
								array_push($repuestas_list, 'correcta');
							}else{
								array_push($repuestas_list, 'incorrecta');
							}
						}
					}
                    break;
                    case '3':
				foreach($pregunta->pregunta_hijo as $key => $_pregunta) {
					$preguntas_lita[] = $_pregunta->titulo.' -> '.$_pregunta->respuestas[0]->opcion;
					$repuesta = $this->preguntas_model->get_respuesta_alumno($_pregunta->id, $intento);
					if ($repuesta) {

						$key = array_search($repuesta->respuesta_id, array_column($_pregunta->respuestas, 'id'));

						$exist_key = in_array($repuesta->respuesta_id, array_column($_pregunta->respuestas, 'id'));

						if ($exist_key) {
							if( $_pregunta->respuestas[$key]->correcta == 1 ){
								array_push($repuestas_list, 'correcta');
							}else{
								array_push($repuestas_list, 'incorrecta');
							}
						}else{
							array_push($repuestas_list, 'incorrecta');
						}


					}
				}
                    break;
               }
		}
		return array($preguntas_lita, $repuestas_list);
	}

}
