<?php
class Usuarios extends Admin_Controller {

	private $validation_rules = array(
		'rol_id' => array(
			'field' => 'rol_id',
			'label' => 'Rol',
			'rules' => 'required|integer'
		),

		'nombre' => array(
			'field' => 'nombre',
			'label' => 'Nombre',
			'rules' => 'required'
		),
		'apellidos' => array(
			'field' => 'apellidos',
			'label' => 'Apellidos',
			'rules' => 'required'
		),
		'email' => array(
			'field' => 'email',
			'label' => 'Email',
			'rules' => 'required|valid_email'
		),
		'password' => array(
			'field' => 'password',
			'label' => 'Contraseña',
			'rules' => 'min_length[6]|max_length[20]'
		),
	);


	public function __construct() {
		parent::__construct();
		$this->modulo = 2;
		$this->nombre_modulo = 'Administradores';
	}

	public function index(){
		$data['catalogos'] = [ 'roles' => $this->rol_model->get_opciones_dropdown()];
		$data['filtos'] =   array('' => '' );
		$this->view('usuarios/lista',$data);
	}

	public function get_usuarios(){
		$data['usuario']= array(
			'activo' => isset($_POST['activo']) ? $this->input->post('activo') : '' ,
			'rol_id'=> isset($_POST['rol_id']) ? $this->input->post('rol_id') : '' ,
			'buscar' => isset($_POST['buscar']) ? $this->input->post('buscar') : ''
		);
		$this->session->set_userdata($data);
		echo json_encode($this->administrador_model->get_usuarios($_POST));
	}

	public function get_all_clientes(){
		$clientes = $this->administrador_model->get_all(['rol_id' => 2]);
		echo json_encode($clientes);
	}

	public function agregar() {

		!$this->acl->permiso('edita_usuarios') ? redirect('/admin/usuarios') : true;
		$this->load->library('form_validation');
		$this->load->helper('form');

		$this->validation_rules['email']['rules'] .= '|callback__email_check';
		$this->validation_rules['password']['rules'] .= '|required';
		if($this->input->post('rol_id') == 2){
			$this->form_validation->set_rules('username', 'Marca', 'required');
		}

		$this->form_validation->set_rules('password', 'Contraseña', 'required|min_length[8]');
		$this->form_validation->set_rules('conf_password', 'Confirmar Contraseña', 'trim|required|matches[password]');

		$this->form_validation->set_rules($this->validation_rules);

		$data['clientes'] = $this->administrador_model->get_all(array('rol_id' => 2));

		// _dump($this->db->last_Query());
		// _dump($clientes);

		$this->breadcrumbs->push('Agregar', 'usuarios/agregar');
		if ($this->form_validation->run() === FALSE) {
			$this->session->set_flashdata('errores', validation_errors());
			$data['roles_dpdwn'] = $this->rol_model->get_opciones_dropdown();

			$this->add_asset('css', 'css/plugins/switchery/switchery.css');
			$this->add_asset('js', 'js/plugins/switchery/switchery.js');
			$this->add_asset('js', 'js/usuarios.js');

			$this->view('usuarios/_agregar', $data);
		}
		else {
			$save['rol_id'] = $this->input->post('rol_id');
			$save['nombre'] = $this->input->post('nombre');
			$save['activo'] = (int)$this->input->post('activo');
			$save['apellidos'] = $this->input->post('apellidos');
			$save['email'] = $this->input->post('email');
			$save['username'] = $this->input->post('username');
			$save['password_hashed'] = $this->auth->hash_password($this->input->post('password'));

			if ($this->input->post('empresa_id')) {
				$save['empresa_id'] = $this->input->post('empresa_id');
			}

			$id = $this->administrador_model->insert($save);
			if ($id) {
				$this->session->set_flashdata('mensajes', 'Nuevo usuario guardado.');
			} else {
				$this->session->set_flashdata('errores', 'Error: No se pudo guardar el nuevo usuario.');
			}

			redirect('usuarios');
		}

	}


	public function editar($id = 0) {
		$this->load->library('form_validation');
		$this->load->helper('form');

		if (!($usuario = $this->administrador_model->get_by_id($id))) {
			$this->session->set_flashdata('errores', 'No se encontro el usuario solicitado.');
			redirect('admin/usuarios');
		}

		if($this->input->post('rol_id') == 2){
			$this->form_validation->set_rules('username', 'Marca', 'required');
		}

		if (!empty($this->input->post('password')) || !empty($this->input->post('conf_password'))) {
			$this->form_validation->set_rules('password', 'Contraseña', 'required|min_length[8]');
	          $this->form_validation->set_rules('conf_password', 'Confirmar Contraseña', 'trim|required|matches[password]');
		}

		// verificamos si se intenta cambiar el email
		if ($usuario->email != $this->input->post('email')) {
			$this->validation_rules['email']['rules'] .= '|callback__email_check';
		}

		// validaciones
		$this->form_validation->set_rules($this->validation_rules);

		$data['clientes'] = $this->administrador_model->get_all(array('rol_id' => 2));


		$this->breadcrumbs->push('Editar', 'usuarios/editar/'.$id);
		if ($this->form_validation->run() === true) {
			$update['nombre'] = $this->input->post('nombre');
			$update['apellidos'] = $this->input->post('apellidos');
			$update['email'] = $this->input->post('email');
			$update['activo'] = (int)$this->input->post('activo');
			$update['username'] = $this->input->post('username');
			// solo se permite asignar el rol de admin a los administradores
			$update['rol_id'] = $this->input->post('rol_id');



			if ($this->input->post('empresa_id')) {
				$update['empresa_id'] = $this->input->post('empresa_id');
			}
			// Password provided, hash it for storage
			if ($this->input->post('password')) {
				$update['password_hashed'] = $this->auth->hash_password($this->input->post('password'));
			}

			if(!$this->acl->permiso('edita_usuarios')){
				$this->session->set_flashdata('errores', 'No fue posible actualizar los datos: Tú no tienes los permisos necesarios.');
				redirect('/admin/usuarios');
			}

			if ($this->administrador_model->update($id, $update)) {
				$this->session->set_flashdata('mensajes', 'Los datos han sido actualizados');
			} else {
				$this->session->set_flashdata('errores', 'No fue posible actualizar los datos.');
			}

			redirect('usuarios');
		} else {

			$data['usuario'] = $usuario;
			$data['roles_dpdwn'] = $this->rol_model->get_opciones_dropdown();
			if (validation_errors()) {
				$this->session->set_flashdata('errores', validation_errors());
			}

			$this->add_asset('css', 'css/plugins/switchery/switchery.css');
			$this->add_asset('js', 'js/plugins/switchery/switchery.js');
			$this->add_asset('js', 'js/usuarios.js');

			$this->view('usuarios/_editar', $data);
		}

	}

	public function _username_check() {
	  if ($this->auth->existe_username($this->input->post('username'))) {
		 $this->form_validation->set_message('_username_check', 'El username ya existe');
		 return false;
	  }
	  return true;
   }

   public function _email_check() {
	  if ($this->auth->existe_email($this->input->post('email'))) {
		 $this->form_validation->set_message('_email_check', 'El email ya es usado por otra cuenta.');
		 return false;
	  }
	  return true;
   }

   public function eliminar($id) {
	  $id = (int) $id;
	  if ($id == 1) {
		 show_error('La operación solicitada no puede ser realizada', 403);
	  }
	  $mensaje  = array('mensaje' => 'Ocurrio un error', 'error' => true);
	  if ($this->administrador_model->delete($id)) {
		  $mensaje  = array('mensaje' => 'Exito al eliminar al usuario', 'error' => false);

	  }
	  echo json_encode($mensaje);
   }

}
