<?php
class Cursos extends Admin_Controller {

     private $validation_rules_curso = [
          'nombre' => [
               'field' => 'nombre',
               'label' => 'Nombre curso',
               'rules' => 'required'
          ],
          'nombre' => [
               'field' => 'nombre_corto',
               'label' => 'Nombre corto curso',
               'rules' => 'required'
          ]
     ];

     private $validation_rules_presentacion = [
          'titulo' => [
               'field' => 'titulo',
               'label' => 'Título',
               'rules' => 'required'
          ]
     ];


     public function __construct() {
          parent::__construct();
          $this->modulo = 4;
          $this->nombre_modulo = 'Cursos';
          $this->load->model(array('rol_model', 'permiso_model', 'administrador_model', 'cursos_model', 'alumnos_model', 'preguntas_model'));
          $this->breadcrumbs->push('Cursos', 'admin/cursos');
          $this->seccion = '7';
          $this->load->library('form_validation');

     }

     public function index(){
          $data['filtos'] =   array('buscar' => '' );
          $this->view('cursos/lista', $data);
     }

     public function eliminar_registro_curso_cliente(){
          $curso = $this->input->post('curso_id');
          $cliente = $this->input->post('cliente');
          $alumnos = $this->alumnos_model->get_alumno_cliente($cliente);
          foreach ($alumnos as $key => $alumno) {
               $incritos = $this->alumnos_model->get_intentos_alumno_curso($alumno->id, $curso);
               if (count($incritos) > 0) {
                    foreach ($incritos as $key => $inscrito) {
                          $this->db->insert('back_intentos', $inscrito);
                          $this->db->where('id', $inscrito->id);
                          $this->db->delete('intentos');
                    }
               }
          }
          echo json_encode(array('type' => 'exito' , 'mensaje' => 'Éxito al reiniciar el curso'));
     }

     public function get_cursos(){
          $data['usuario']= array(
               'activo' => isset($_POST['activo']) ? $this->input->post('activo') : '' ,
               'buscar' => isset($_POST['buscar']) ? $this->input->post('buscar') : ''
          );
          echo json_encode($this->cursos_model->get_cursos($_POST), true);
          // _dump($this->db->last_query());
     }

     public function form($id = false){
          $data = array(
               'id' => '' ,
               'nombre' => '' ,
               'nombre_corto' => '' ,
               'descripcion' => '' ,
               'imagen' => 'sinimagen.jpg' ,
               'estatus' => '',
               'curos_caduca' => '',
               'prog_baja' => '',
               'fecha_alta' => '',
          );

          if ($id) {
               $data = array_merge($data, (array)$this->cursos_model->get_curso_id($id));
          }
          $form['estatus']  = $this->cursos_model->estatus_cursos();
          $form['categorias']  = $this->cursos_model->get_categorias_cursos();
          $form['form'] = $data;
          $this->view('cursos/form', $form);
     }

     public function eliminar_curso($curso_id){
          $mensaje  = array('mensaje' => 'Ocurrio un error', 'error' => true);
          if ($this->cursos_model->delete_curso($curso_id)) {
               $mensaje  = array('mensaje' => 'Exito al eliminar al usuario', 'error' => false);
          }
          echo json_encode($mensaje);
     }


     public function eliminar_material(){
          $material = $this->input->post('material_id');
          $curso_id = $this->cursos_model->get_material_id($material)->curso_id;
          $mensaje  = array('mensaje' => 'Ocurrio un error', 'error' => true);
          if ($this->cursos_model->eliminar_material($material)) {
               $this->ordenar_material($curso_id);
               $mensaje  = array('mensaje' => 'Exito al eliminar al usuario', 'error' => false);
          }
          echo json_encode($mensaje);
     }

     public function data_form(){
          $this->form_validation->set_rules('nombre', 'Nombre curso', 'required');
          $this->form_validation->set_rules('nombre_corto', 'Nombre corto curso', 'required');
          $this->form_validation->set_rules('estatus', 'Estatus', 'required');
          $this->form_validation->set_rules('estatus', 'Estatus', 'required');
          $this->form_validation->set_rules('categoria_id', 'Categoria', 'required');

          if ($this->input->post('estatus') == 2) {
               $this->form_validation->set_rules('fecha_alta', 'Fecha alta', 'required');;
          }
          if ($this->input->post('prog_baja') == 'true') {
               $this->form_validation->set_rules('curos_caduca', 'Fecha baja', 'required');;
          }

          if ($this->input->post('se_califica') == 'true') {
               $this->form_validation->set_rules('calificacion_aprovatoria', 'Calificación aprovatoria', 'required');;
          }

          // _dump($_POST);
          // die;

          if ($this->form_validation->run()) {
               $curso['nombre'] = $this->input->post('nombre');
               $curso['nombre_corto'] = $this->input->post('nombre_corto');
               $curso['descripcion'] = $this->input->post('descripcion');
               $curso['duracion'] = $this->input->post('duracion');
               $curso['imagen'] = $this->input->post('imagen');
               $curso['imagen_small'] = $this->input->post('imagen_small');
               $curso['tipo_archivo'] = $this->input->post('tipo_archivo');
               $curso['categoria_id'] = $this->input->post('categoria_id');

               $curso['estatus'] = $this->input->post('estatus');
               $curso['num_intentos'] = $this->input->post('num_intentos');
               $curso['prog_baja'] = $this->input->post('prog_baja') == 'true' ? 1 : 0;
               $curso['se_califica'] = $this->input->post('se_califica') == 'true' ? 1 : 0;
               $curso['calificacion_aprovatoria'] = $this->input->post('calificacion_aprovatoria');
               if ($this->input->post('prog_baja') == 'true') {
                    $curso['curos_caduca'] = implode("-", array_reverse(explode("/", $this->input->post('curos_caduca'))));
               }else{
                    $curso['curos_caduca'] = '';
               }
               $curso['fecha_alta'] = $this->input->post('fecha_alta');
               $curso['cliente_id'] = 2;


               if ( $this->input->post('id')) {
                    $curso['fecha_actualizacion'] = date('Y-m-d H:m:s');
                    $this->cursos_model->edit_curso($this->input->post('id'), $curso);
                    $this->session->set_flashdata('mensajes', 'Se ha actualizado el curso ');
                    $res = [
                         'status' => 'ok',
                    ];
               }else{
                    $curso['fecha_creacion'] = date('Y-m-d H:m:s');
                    $curso_id = $this->cursos_model->new_curso($curso);
                    if ($this->rol_id_user == 2 || $this->rol_id_user == 3) {
                         if ($this->rol_id_user == 2) {
                              $registro['admin_id'] = $this->session->userdata('admin')['usuario_id'];
                         }
                         if ($this->rol_id_user == 3) {
                              $registro['admin_id'] = $this->session->userdata('admin')['empresa_id'];
                         }
                         $registro['curso_id'] = $curso_id;
                         $registro['creado'] = date('Y-m-d');
                         $this->cursos_model->registrar_curso_cliente($registro);
                    }
                    $this->session->set_flashdata('mensajes', 'Se ha creado el curso ');
                    $res = [
                         'status' => 'ok',
                    ];
               }

               echo json_encode($res);
          } else {
               $res = [
                    'status' => 'error',
                    // 'errores' => validation_errors()
                    'errores' =>  $this->form_validation->error_array()
               ];
               echo json_encode($res);
          }
     }

     public function reporte($id = false){
          $data = array();
          $data['inscritos'] = $this->alumnos_model->alumnos_curso($id);
          // _dump($this->db->last_query());
          $data['id'] = $id;
          $data['aprobados'] = $this->alumnos_model->alumnos_curso($id, 1);
          // _dump($this->db->last_query());

          $data['reprobados'] = $this->alumnos_model->alumnos_curso($id, 2);
          // _dump($this->db->last_query());

          $data['curso'] =$this->cursos_model->get_curso_id($id);
          // _dump($data);
          $this->view('cursos/dashboard', $data);
     }

     public function media($id = false){
          $data = array(
               'material_introductorio' => array(
                    'id' => '',
                    'curso_id' => $id,
                    'titulo' => '',
                    'descripcion' => '',
                    'tipo_archivo' => '',
                    'archivo' => '',
               )
          );

          $materia_intro =  $this->cursos_model->get_presentacion_curso($id);
          if ($materia_intro) {
               $data['material_introductorio'] = array_merge($data['material_introductorio'], (array)$materia_intro);
          }
          $data['catalogo_preguntas'] = $this->cursos_model->get_catalogo_preguntas();
          $data['curso_id'] = $id;


          $this->view('cursos/recurso', $data);
     }

     public function save_material_introductorio(){
          $this->form_validation->set_rules('titulo', 'Titulo', 'required');
          if ($this->form_validation->run()) {
               $presentacion['curso_id'] = $this->input->post('curso_id');
               $presentacion['titulo'] = $this->input->post('titulo');
               $presentacion['descripcion'] = $this->input->post('descripcion');
               $presentacion['tipo_archivo'] = $this->input->post('tipo_archivo');
               $presentacion['archivo'] = $this->input->post('archivo');
               if ( $this->input->post('id')) {
                    $this->cursos_model->editar_presentacion($this->input->post('id'), $presentacion);
                    $this->session->set_flashdata('mensajes', 'Se ha actualizado la prensentación ');
                    $res = [
                         'status' => 'ok',
                    ];
               }else{
                    $this->cursos_model->insertar_presentacion($presentacion);
                    $this->session->set_flashdata('mensajes', 'Se ha creado la presentación ');
                    $res = [
                         'status' => 'ok',
                    ];
               }

               echo json_encode($res);
          } else {
               $res = [
                    'status' => 'error',
                    'errores' => validation_errors()
               ];
               echo json_encode($res);
          }
     }

     public function get_modulos_curos_id(){
          $curso_id = $this->input->post('cursoId');
          echo json_encode($this->cursos_model->get_modulos_curoso($curso_id));
     }

     public function get_modulo_id(){
          $id = $this->input->post('id');
          if ($id == 'false' ) {
               $arrayName = array(
                         "id" => "",
                         "curso_id" => "",
                         "titulo" => "",
                         "orden" => "",
                         "doctors" => "",
                         "duracion" => ""
                     );
                     echo json_encode($arrayName);
          }else{
               echo json_encode($this->cursos_model->get_modulos_id($id));
          }
     }


     public function save_modulo(){
          $this->form_validation->set_rules('titulo', 'Titulo', 'required');
          if ($this->form_validation->run()) {
               $modulo['orden']  = 0 ;
               $modulo['curso_id'] = $this->input->post('cursoid');
               $modulo['doctors'] = $this->input->post('doctors');
               $modulo['duracion'] = $this->input->post('duracion');
               $modulo['titulo'] = $this->input->post('titulo');
               $ultimo = $this->db->select('max(orden)')->where('curso_id', $modulo['curso_id'])->get('secciones')->row_array();
               $modulo['orden'] = ++$ultimo['max(orden)'] ;
               if ( $this->input->post('id')) {
                    $this->cursos_model->editar_modulo_curso($this->input->post('id'), $modulo);
                    $this->session->set_flashdata('mensajes', 'Se ha actualizado la prensentación ');
                    $res = [
                         'status' => 'ok',
                    ];
                    $this->ordenar_modulos($modulo['curso_id']);
               }else{
                    $this->cursos_model->insertar_modulo_curso($modulo);
                    $this->session->set_flashdata('mensajes', 'Se ha creado la presentación ');
                    $res = [
                         'status' => 'ok',
                    ];
               }

               echo json_encode($res);
          } else {
               $res = [
                    'status' => 'error',
                    'errores' => validation_errors()
               ];
               echo json_encode($res);
          }
     }


     public function save_material(){
          $this->form_validation->set_rules('titulo', 'Titulo', 'required');
          if ($this->form_validation->run()) {
               $material['curso_id'] = $this->input->post('curso_id');
               $material['titulo'] = $this->input->post('titulo');
               $material['seccion_id'] = $this->input->post('seccion_id');
               $material['descripcion'] = $this->input->post('descripcion');
               $material['tipo_archivo'] = $this->input->post('tipo_archivo');
               $material['archivo'] = $this->input->post('archivo');
               $material['contenido'] = $this->input->post('contenido');
               $material['duracion'] = $this->input->post('duracion');
               $material['doctores'] = $this->input->post('doctores');
               $material['objetivos'] = $this->input->post('objetivos');
               $material['termino'] = $this->input->post('termino');
               $material['infografia'] = $this->input->post('infografia');
               $material['referencias'] = $this->input->post('referencias');
               $ultimo = $this->db->select('max(orden)')->where('curso_id', $material['curso_id'])->get('materiales')->row_array();
               $material['orden'] = ++$ultimo['max(orden)'] ;
               if ( $this->input->post('id')) {
                    $this->cursos_model->editar_material_modulo($this->input->post('id'), $material);
                    $this->session->set_flashdata('mensajes', 'Se ha actualizado el material ');
                    $res = [
                         'status' => 'ok',
                    ];
                    $this->ordenar_material($material['curso_id']);
               }else{
                    $this->cursos_model->insertar_material_modulo($material);
                    $this->session->set_flashdata('mensajes', 'Se ha creado la material ');
                    $res = [
                         'status' => 'ok',
                    ];
               }

               echo json_encode($res);
          } else {
               $res = [
                    'status' => 'error',
                    'errores' => validation_errors()
               ];
               echo json_encode($res);
          }
     }


     public function get_material_id(){
          $id = $this->input->post('id');
          if ($id == 'false' ) {
               $arrayName = array(
                         "id" => "",
                         "curso_id" => "",
                         "titulo" => "",
                         "seccion_id" => "",
                         "descripcion" => "",
                         "tipo_archivo" => "",
                         "archivo" => "",
                         "contenido" => "",
                         "tipo" => "imagen",
                         "duracion" => "",
                         "doctores" => "",
                         "objetivos" => "",
                         "termino" => "",
                         "infografia" => "",
                         "regerenias" => "",
                     );
                     echo json_encode($arrayName);
          }else{
               echo json_encode($this->cursos_model->get_material_id($id));
          }
     }

     public function eliminar_modulo() {
          $id = (int)$this->input->post('modulo_id');
          $modulo_id = $this->cursos_model->get_modulos_id($id)->curso_id;
          $mensaje  = array('mensaje' => 'Ocurrio un error', 'error' => true);
          if ($this->cursos_model->delete_modulos($id)) {
               $materiales = $this->cursos_model->get_material_modulo($id);
               foreach ($materiales as $key => $material) {
                    $this->cursos_model->eliminar_material($material->id);
               }
               $mensaje  = array('mensaje' => 'Exito al eliminar al usuario', 'error' => false);
          }
          $this->ordenar_modulos($modulo_id);
          echo json_encode($mensaje);
     }

     public function get_pregunta_id(){
          $id = (int)$this->input->post('material_id');
     }

     public function upload(){
          $config['upload_path'] = DIR_UPLOAD.'/principal';
          // $config['upload_path'] = './uploads';
          $config['allowed_types'] = 'gif|jpg|png|jpeg|mp4';
          $config['encrypt_name'] = TRUE;
          // $config['max_width'] = 330;
          // $config['max_height'] = 250;
          $this->load->library('upload', $config);
          $mensaje = array('data' => array('end_offset' => '', 'mensaje' =>  "" ), 'status' => 'error');
          if ( ! $this->upload->do_upload('file')){
               $mensaje['data']['mensaje'] =  $this->upload->display_errors();
          }else{
               $mensaje['data']['mensaje'] =  'Exito al subir el archivo';
               $mensaje['data']['archivo'] =  $this->upload->data('file_name');
               if ($this->upload->data('file_type') == 'video/mp4' ) {
                    $mensaje['data']['tipo'] =  'video';
               }else{
                    $mensaje['data']['tipo'] =  'imagen';
               }

               $mensaje['status'] = 'exito';
          }
          echo json_encode($mensaje);
     }


     public function upload_presentacion(){
          $config['upload_path'] = DIR_UPLOAD.'/principal';
          $config['allowed_types'] = 'gif|jpg|png|jpeg|mp4';
          $config['encrypt_name'] = TRUE;
          $this->load->library('upload', $config);
          $mensaje = array('data' => array('end_offset' => '', 'mensaje' =>  "" ), 'status' => 'error');
          if ( ! $this->upload->do_upload('file')){
               $mensaje['data']['mensaje'] =  $this->upload->display_errors();
          }else{
               $mensaje['data']['mensaje'] =  'Exito al subir el archivo';
               $mensaje['data']['archivo'] =  $this->upload->data('file_name');
               if($this->upload->data('file_type') == 'video/mp4' ) {
                    $mensaje['data']['tipo'] =  'video';
               }else{
                    $mensaje['data']['tipo'] =  'img';
               }
               $mensaje['status'] = 'exito';
          }
          echo json_encode($mensaje);
     }

     public function upload_material(){
          $config['upload_path'] = DIR_UPLOAD.'/video';
          // $config['allowed_types'] = 'mp4|pdf';
          $config['allowed_types'] = 'mp4|pptx|ppt|xlsx|xlsm|docx|doc|xls|docm|xml|xls|pdf';

          $config['encrypt_name'] = TRUE;
          // $config['upload_path'] = './uploads';

          $this->load->library('upload', $config);
          $mensaje = array('data' => array('end_offset' => '', 'mensaje' =>  "" ), 'status' => 'error');
          if ( ! $this->upload->do_upload('file')){
               $mensaje['data']['mensaje'] =  $this->upload->display_errors();
          }else{
               $mensaje['data']['mensaje'] =  'Exito al subir el archivo';
               $mensaje['data']['archivo'] =  $this->upload->data('file_name');
               // _dump($this->upload->data('file_type'));
               if ($this->upload->data('file_type') == 'application/pdf') {
                    $mensaje['data']['tipo'] =  'pdf';
               }
               elseif ( $this->upload->data('file_type') == 'video/mp4' ) {
                    $mensaje['data']['tipo'] =  'video';
               }else{
                    $mensaje['data']['tipo'] =  'office';
               }

               $mensaje['status'] = 'exito';
          }
          echo json_encode($mensaje);
     }

     public function upload_relacionados(){
          $config['upload_path'] = DIR_UPLOAD.'/relacionados';
          // $config['upload_path'] = './uploads';

          $config['allowed_types'] = 'mp4';
          $config['encrypt_name'] = TRUE;
          $this->load->library('upload', $config);
          $mensaje = array('data' => array('end_offset' => '', 'mensaje' =>  "" ), 'status' => 'error');
          if ( ! $this->upload->do_upload('file')){
               $mensaje['data']['mensaje'] =  $this->upload->display_errors();
          }else{
               $mensaje['data']['mensaje'] =  'Exito al subir el archivo';
               $mensaje['data']['archivo'] =  $this->upload->data('file_name');
               $mensaje['status'] = 'exito';
          }
          echo json_encode($mensaje);
     }

     public function upload_profesores(){
          $config['upload_path'] = DIR_UPLOAD.'/profesores';
          $config['allowed_types'] = 'gif|jpg|png|jpeg';
          $config['encrypt_name'] = TRUE;
          $this->load->library('upload', $config);
          $mensaje = array('data' => array('end_offset' => '', 'mensaje' =>  "" ), 'status' => 'error');
          if ( ! $this->upload->do_upload('file')){
               $mensaje['data']['mensaje'] =  $this->upload->display_errors();
          }else{
               $mensaje['data']['mensaje'] =  'Exito al subir el archivo';
               $mensaje['data']['archivo'] =  $this->upload->data('file_name');
               $mensaje['status'] = 'exito';
          }
          echo json_encode($mensaje);
     }


     public function get_all_cursos(){
          echo json_encode($this->cursos_model->get_all($_POST), true);
     }

     public function cliente_inscrito_en_curso(){
		$curso_id = $this->input->post('curso_id');
		$cliente_id = $this->input->post('cliente');
		echo json_encode($this->administrador_model->validar_cliente_in_curso($cliente_id, $curso_id));
	}

	public function inscribir_en_curso(){
		$curso_id = $this->input->post('curso_id');
		$cliente_id = $this->input->post('cliente');
		$inscrito = $this->administrador_model->validar_cliente_in_curso($cliente_id, $curso_id);
		if ($inscrito) {
			$this->administrador_model->eliminar_cliente_curso_inscrito($cliente_id, $curso_id);
			$mensaje['mensaje'] =  'Baja del curso';
		}else{
			$save['admin_id'] = $cliente_id;
			$save['curso_id'] = $curso_id;
			$this->administrador_model->inser_cliente_curso_inscrito($save);
			$mensaje['mensaje'] =  'Inscrito al curso';

		}
		$mensaje['errores'] =  '';
		echo json_encode($mensaje);
	}


     public function get_cursos_cliente(){
          $cliente_id = $this->input->post('cliente_id');
          $cursos = $this->cursos_model->get_cursos_cliente($cliente_id);
          echo json_encode($cursos);
     }


     public function ordenar_modulos($curso_id){
          $modulos = $this->cursos_model->get_modulos_curoso($curso_id);
          $orden = 1;
          foreach ($modulos as $key => $modulo) {
               $save['orden'] = $orden;
               $this->cursos_model->editar_modulo_curso($modulo->id, $save);
               $orden++;
          }
     }

     public function ordenar_material($curso_id){
          $modulos = $this->cursos_model->get_modulos_curoso($curso_id);
          $orden = 1;
          foreach ($modulos as $key => $modulo) {
               foreach ($modulo->materiales as $key => $materiales) {
                    $save['orden'] = $orden;
                    $this->cursos_model->editar_material_modulo($materiales->id, $save);
                    $orden++;
               }
          }
     }

     public function orden_manual()
     {
          $list = $this->input->post('list');
          $ord_modul = 1;
          $ord_mat = 1;
          foreach ($list as $key => $modulos) {
               $mod['orden'] = $ord_modul;
               $this->cursos_model->editar_modulo_curso($modulos['id'], $mod);
               foreach ($modulos['materiales'] as $key => $material) {
                    $mat['orden'] = $ord_mat;
                    $this->cursos_model->editar_material_modulo($material['id'], $mat);
                    ++$ord_mat;
               }
               ++$ord_modul;
          }

     }

     public function clonar_curso($curso_id)
     {
          $this->view('cursos/clonando', array());

          $this->load->model(array('cursos_model', 'cursos_model', 'alumnos_model', 'preguntas_model', 'examen_model', 'profesores_model', 'relacionados_model'));

          $curso = $this->cursos_model->get_curso_id($curso_id);
          unset($curso['id']);
          $curso['nombre'] = 'Copia -'. $curso['nombre'];
          // echo "<br>--------Curso----------<br>";
          $new_curso_id = $this->cursos_model->new_curso($curso);

          $presentacion = $this->cursos_model->get_presentacion_curso($curso_id);
          // echo "<br>--------Presentacion----------<br>";
          $presentacion = json_decode(json_encode($presentacion), true);
          $presentacion['curso_id'] = $new_curso_id;
          unset($presentacion['id']);
          $this->cursos_model->insertar_presentacion($presentacion);


          $modulos = $this->cursos_model->get_modulos_curoso($curso_id);
          $modulos = json_decode(json_encode($modulos), true);
          foreach ($modulos as $key => $modulo) {
               // echo "<br>--------Modulo----------<br>";
               $new_modulo = $modulo;
               $new_modulo['curso_id'] = $new_curso_id;
               unset($new_modulo['id']);
               unset($new_modulo['materiales']);
               $new_modulo_id = $this->cursos_model->insertar_modulo_curso($new_modulo);
               foreach ($modulo['materiales'] as $key => $material) {
                    // echo "<br>--------Material----------<br>";
                    $new_material = $material;
                    $new_material['curso_id'] = $new_curso_id;
                    $new_material['seccion_id'] = $new_modulo_id;
                    unset($new_material['id']);
                    $new_material_id = $this->cursos_model->insertar_material_modulo($new_material);
                    $preguntas = $this->preguntas_model->get_preguntas_materiales($material['id']);
                    $preguntas = json_decode(json_encode($preguntas), true);
                    foreach ($preguntas as $key => $pregunta) {
                         // echo "<br>--------Pregunta----------<br>";
                         $new_pregunta = $pregunta;
                         $new_pregunta['material_id'] = $new_material_id;
                         unset($new_pregunta['id']);
                         $new_pregunta_id = $this->preguntas_model->insertar_pregunta($new_pregunta);
                         if (isset($preguntas['respuestas'])) {
                              foreach ($preguntas['respuestas'] as $key => $respuesta) {
                                   // echo "<br>--------Respuesta----------<br>";
                                   unset($respuesta['id']);
                                   $respuesta['pregunta_id'] = $new_pregunta_id;
                                   $this->preguntas_model->insertar_respuesta($respuesta);
                              }
                         }
                    }
               }
          }

          $examenes = $this->examen_model->get_examenes_curso($curso_id);
          $examenes = json_decode(json_encode($examenes), true);

          foreach ($examenes as $key => $examen) {
               // echo "<br>--------Examen----------<br>";
               $new_examen = $examen;
               $new_examen['curso_id'] = $new_curso_id;
               unset($new_examen['id']);
               $new_examen_id = $this->examen_model->new_examen($new_examen);

               $preguntas = $this->preguntas_model->get_preguntas_examen($examen['id']);
               $preguntas = json_decode(json_encode($preguntas), true);

               foreach ($preguntas as $key => $pregunta) {
                    // echo "<br>--------Pregunta----------<br>";
                    $new_pregunta = $pregunta;
                    $new_pregunta['examen_id'] = $new_examen_id;
                    unset($new_pregunta['id']);
                    unset($new_pregunta['respuestas']);
                    unset($new_pregunta['pregunta_hijo']);

                    $new_pregunta_id = $this->preguntas_model->insertar_pregunta($new_pregunta);
                    if ($pregunta['catalogo_preguntas'] == 1 || $pregunta['catalogo_preguntas'] == 4 || $pregunta['catalogo_preguntas'] == 6 || $pregunta['catalogo_preguntas'] == 5 ) {
                         foreach ($pregunta['respuestas'] as $key => $respuesta) {
                              // echo "<br>--------Respuesta----------<br>";
                             unset($respuesta['id']);
                             $respuesta['pregunta_id'] = $new_pregunta_id;
                             $this->preguntas_model->insertar_respuesta($respuesta);
                         }
                    }
                    if ($pregunta['catalogo_preguntas'] == 2 || $pregunta['catalogo_preguntas'] == 3) {
                         foreach ($pregunta['pregunta_hijo'] as $key => $preguntas_hijo) {
                              // echo "<br>--------Pregunta hijo----------<br>";

                              $new_preguntas_hijo = $preguntas_hijo;
                              $new_preguntas_hijo['pregunta_padre'] = $new_pregunta_id;
                              unset($new_preguntas_hijo['id']);
                              unset($new_preguntas_hijo['respuestas']);
                              $new_preguntas_hijo_id = $this->preguntas_model->insertar_pregunta($new_preguntas_hijo);
                              foreach ($preguntas_hijo['respuestas'] as $key => $respuestas_new) {
                                   // echo "<br>--------Respuestas----------<br>";
                                  unset($respuestas_new['id']);
                                  $respuestas_new['pregunta_id'] = $new_preguntas_hijo_id;
                                  $this->preguntas_model->insertar_respuesta($respuestas_new);

                              }
                         }
                    }
               }
          }

          $facilitadores = $this->profesores_model->get_profesor($curso_id);
          $facilitadores = json_decode(json_encode($facilitadores), true);
          foreach ($facilitadores as $key => $facilitador) {
               // echo "<br>--------Respuestas----------<br>";
               unset($facilitador['id']);
               $facilitador['curso_id'] = $new_curso_id;
               $this->profesores_model->new_profesor($facilitador);
          }

          $relacionados = $this->relacionados_model->get_relacionados($curso_id);
          $relacionados = json_decode(json_encode($relacionados), true);
          foreach ($relacionados as $key => $relacionado) {
               // echo "<br>--------Relacionados----------<br>";
               unset($relacionado['id']);
               $relacionado['curso_id'] = $new_curso_id;
               $this->relacionados_model->new_relacionados($relacionado);
          }


          redirect('/cursos');

     }

}
