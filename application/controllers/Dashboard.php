<?php
class Dashboard extends Admin_Controller {

	public function __construct() {
		parent::__construct();
		$this->modulo = 1;
		$this->nombre_modulo = 'Panel de control';
		$this->load->model(array('cursos_model', 'cursos_model', 'alumnos_model'));
		
	}

	public function index(){
		$data['usuarios'] = $this->administrador_model->count_by([]);
		$data['cursos'] = $this->cursos_model->count_by([]);
		$data['alumnos'] = $this->alumnos_model->count_by([]);
		$this->view('dashboard', $data);
	}
}
