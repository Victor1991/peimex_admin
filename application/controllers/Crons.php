<?php
class Crons extends Public_Controller {

     public function __construct() {
          parent::__construct();
          $this->load->model(array('cursos_model', 'alumnos_model', 'preguntas_model'));
     }

     public function activar_desactivar_cursos(){
          $cursos = $this->cursos_model->get_all(array());
          // Damos de alta
          foreach ($cursos as $key => $curso) {
               if ($curso->estatus == 2) {
                    if ( date('Y-m-d') == $curso->fecha_alta ) {
                         $save['estatus'] = 3;
                         $this->cursos_model->edit_curso($curso->id, $save);
                    }
               }
          }
          // Damos de baja
          foreach ($cursos as $key => $curso) {
               if ($curso->prog_baja == 1) {
                    if (date('Y-m-d') == $curso->curos_caduca ) {
                         $save['estatus'] = 4;
                         $this->cursos_model->edit_curso($curso->id, $save);
                    }
               }
          }
     }
}
