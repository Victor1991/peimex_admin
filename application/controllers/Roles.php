<?php
class Roles extends Admin_Controller {

    public function __construct() {
        parent::__construct();

        $this->modulo = 3;
        $this->nombre_modulo = 'Roles';

        $this->load->model(array('rol_model', 'permiso_model'));
        $this->breadcrumbs->push('Roles', 'admin/roles');
        $this->seccion = '7';
    }

    public function index() {
        $data['roles'] = $this->rol_model->get_all();

        foreach ($data['roles'] as &$rol) {
            $rol->num_usuarios = $this->rol_model->num_usuarios($rol->id);
        }

        $this->view('roles/lista', $data);
    }

    public function form($id = false) {
        if($id){
            $data['rol'] = (array)$this->rol_model->get_by_id($id);
            $data['permisos_rol'] = $this->rol_model->get_permiso_by_rol_id($id);
            $data['permisos'] = [
                'sitio' => $this->permiso_model->get_permisos_modulo('sitio'),
                'roles' => $this->permiso_model->get_permisos_modulo('roles'),
                'usuarios' => $this->permiso_model->get_permisos_modulo('usuarios'),

            ];
            $this->add_asset('css', 'css/plugins/toastr/toastr.min.css');
            $this->add_asset('js', 'js/plugins/toastr/toastr.min.js');
            return $this->view('roles/form', $data);
        }
         return show_404();
    }

    public function eliminar($id) {
        $id = (int) $id;
        if ($id == 1) {
            show_error('La operación solicitada no puede ser realizada', 403);
        }

        $rol = $this->rol_model->get_by_id($id);
        if (!$rol) {
            show_404();
        }

        $this->rol_model->eliminar_permisos($id);
        if ($this->rol_model->eliminar($id)) {
            $this->session->set_flashdata('mensajes', 'Se ha eliminado el rol solicitado.');
        } else {
            $this->session->set_flashdata('errores', 'No se ha podido eliminar el rol solicitado');
        }

        redirect('admin/roles');
    }

    public function _nombre_rol_check() {
        if ($this->rol_model->existe_nombre(strip_tags($this->input->post('nombre')), (int)$this->input->post('rol_id'))) {
            $this->form_validation->set_message('_nombre_rol_check', 'Ya existe un rol con el nombre indicado.');
            return false;
        }
        return true;
    }

    public function storage()
    {
        if(!$this->acl->permiso('edita_roles')){
            $respuesta['error'] = true;
            $respuesta['mensaje'] = 'No tienes permiso para realizar esta acción';
            echo json_encode($respuesta);
            return false;
        }
        $this->rol_model->guardar_permisos($_POST['rol']['id'], $_POST['permisos']);
        $respuesta['error'] = false;
        $respuesta['mensaje'] = 'Actualizado';
        $this->session->set_flashdata('mensajes', 'Se ha actualizado con éxito el rol.');
        echo json_encode($respuesta);
    }
}
