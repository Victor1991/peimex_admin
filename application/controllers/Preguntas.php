<?php
class Preguntas extends Admin_Controller {

     public function __construct() {
          parent::__construct();
          $this->load->model(array('rol_model', 'permiso_model', 'preguntas_model'));
          $this->load->library('form_validation');
     }

     public function get_preguntas_material(){
          $id =  $this->input->post('id');
          echo json_encode($this->preguntas_model->get_preguntas_materiales($id));
     }

     public function get_preguntas_examen(){
          $examen_id =  $this->input->post('examen_id');
          echo json_encode($this->preguntas_model->get_preguntas_examen($examen_id), true);
     }

     public function eliminar_respuesta(){
          $respuesta_id  = $this->input->post('respuesta_id');
          $mensaje  = array('mensaje' => 'Ocurrio un error', 'error' => true);
          if ($this->preguntas_model->eliminar_respuesta($respuesta_id)) {
               $mensaje  = array('mensaje' => 'Exito al eliminar la respuesta', 'error' => false);
          }
          echo json_encode($mensaje);
     }

     public function eliminar_pregunta(){
          $pregunta_id  = $this->input->post('pregunta_id');
          $mensaje  = array('mensaje' => 'Ocurrio un error', 'error' => true);
          if ($this->preguntas_model->eliminar_pregunta($pregunta_id)) {
               $mensaje  = array('mensaje' => 'Exito al eliminar la pregunta', 'error' => false);
          }
          echo json_encode($mensaje);
     }

     public function get_pregunta_id(){
          $id =  $this->input->post('id');

          $data = array('pregunta' => array('id' => '', 'titulo' => '', 'descripcion' => '', 'preguntas' => array()), 'respuestas' => array() );
          if ($id != 'false') {
               $data['pregunta'] = (array)$this->preguntas_model->get_pregunta_id($id);
               if ($data['pregunta']['catalogo_preguntas'] == 1 || $data['pregunta']['catalogo_preguntas'] == 4 || $data['pregunta']['catalogo_preguntas'] == 5 || $data['pregunta']['catalogo_preguntas'] == 6 || $data['pregunta']['catalogo_preguntas'] == 0 ) {
                    $data['respuestas'] = (array)$this->preguntas_model->get_respuestas_pregunta($data['pregunta']['id']);
               }
               if ($data['pregunta']['catalogo_preguntas'] == 2 ) {

                    $preguntas_hijo = $this->db->from('preguntas')->where('pregunta_padre', $data['pregunta']['id'])->get()->result();
                         $data['pregunta']['preguntas'] = array();
                    foreach ($preguntas_hijo as $key2 => $pregunta) {
                         $data['pregunta']['preguntas'][$key2] = $pregunta;
                         // $data['pregunta']['pregunta_hijo'][$key2]->respuestas = array();
                         $data['pregunta']['preguntas'][$key2]->respuestas = $this->preguntas_model->get_respuestas_pregunta($pregunta->id);

                    }
               }
               if ( $data['pregunta']['catalogo_preguntas'] == 3 ) {

                    $preguntas_hijo = $this->db->from('preguntas')->where('pregunta_padre', $data['pregunta']['id'])->get()->result();
                         $data['pregunta']['preguntas'] = array();
                    foreach ($preguntas_hijo as $key2 => $pregunta) {
                         $data['pregunta']['preguntas'][$key2] = $pregunta;
                         // $data['pregunta']['pregunta_hijo'][$key2]->respuestas = array();
                         $data['pregunta']['preguntas'][$key2]->respuestas = $this->preguntas_model->get_respuestas_pregunta($pregunta->id);

                    }
               }

          }
          echo json_encode($data);
     }

     public function save_preguntas(){

          $mensaje_error = '';
          $valido = true;

          // _dump($_POST);
          // die;

          // Validar pregunta
          if ($this->input->post('pregunta')) {
               if ($_POST['pregunta']['titulo'] == '') {
                    $mensaje_error .= '<p>Pregunta es requerida</p>';
                    $valido = false;
               }
          }else{
               $mensaje_error .= '<p>Pregunta es requerida</p>';
               $valido = false;
          }

          // Validar respuestas
          if (!isset($_POST['respuestas'])) {
               $mensaje_error .= '<p>Se requieren respuestas</p>';
               $valido = false;
          }else{
               // Validar respuesta correctas
               if ($_POST['res_correcta'] =='') {
                    $mensaje_error .= '<p>Respuesta correcta es requerida</p>';
                    $valido = false;
               }
          }


          if ($valido) {
               $pregunta = $this->input->post('pregunta');
               $respuestas = $this->input->post('respuestas');
               $respuestas = $this->asignar_valor_array($respuestas, 'correcta', '0');
               if (is_array($this->input->post('res_correcta'))) {
                    foreach ($this->input->post('res_correcta') as $key => $resp_correcta) {
                         $respuestas[$resp_correcta]['correcta'] = 1;
                    }
               }else{
                    $respuestas[$this->input->post('res_correcta')]['correcta'] = 1;
               }

               if ((int)$pregunta['id'] !== 0) {
                    $pregunta_id = $pregunta['id'];
                    $termino = $this->preguntas_model->editar_pregunta($pregunta['id'], $pregunta);
                    $no_eliminar = [];
                    foreach ($respuestas as $key => $respuestas) {
                         if ($respuestas['id']) {
                              $insertado = $this->preguntas_model->editar_respuesta($respuestas['id'], $respuestas);
                              array_push($no_eliminar, $respuestas['id']);
                         }else {
                              unset($respuestas['id']);
                              $insertado = $this->preguntas_model->insertar_respuesta($respuestas);
                              array_push($no_eliminar, $insertado);
                         }
                    }

                    $this->preguntas_model->eliminar_respuestas($no_eliminar, $pregunta_id);

                    if ($termino) {
                         $res = [
                              'error' => false,
                              'mensaje' => 'Pregunta editada correctamente.'
                         ];
                         echo json_encode($res);
                    }


               }else{

                    if(isset($pregunta['examen_id'])){
                         $this->db->where('examen_id', $pregunta['examen_id']);
                    }else {
                         $this->db->where('material_id', $pregunta['material_id']);
                    }

                    $ultimo = $this->db->select('max(orden)')->get('preguntas')->row_array();
                    if ($ultimo) {
                         $pregunta['orden'] = ++$ultimo['max(orden)'] ;
                    }else {
                         $pregunta['orden'] = 1;
                    }


                    $pregunta_id = $this->preguntas_model->insertar_pregunta($pregunta);
                    $respuestas = $this->asignar_valor_array($respuestas, 'pregunta_id', $pregunta_id);
                    $termino = $this->preguntas_model->insert_bach($respuestas);
                    if ($termino) {
                         $res = [
                              'error' => false,
                              'mensaje' => 'Pregunta guardada correctamente.'
                         ];
                         echo json_encode($res);
                    }
               }
          } else {
               $res = [
                    'error' => true,
                    'errores' => $mensaje_error
               ];
               echo json_encode($res);
          }
     }

     public function save_preguntas_completar(){
               // _dump($_POST);
               // die;
          $save['titulo'] = $this->input->post('titulo');
          $save['descripcion'] = $this->input->post('descripcion');
          $save['catalogo_preguntas'] = $this->input->post('catalogo_preguntas');
          $save['examen_id'] = $this->input->post('examen_id');
          $preguntas = $this->input->post('preguntas');
          $validacion = $this->validar_respuetas_correctas($preguntas);
          if ($validacion['error'] === false) {
               if ((int)$this->input->post('id') != 0) {
                    $termino = $this->preguntas_model->editar_pregunta($this->input->post('id'), $save);
                    $pregunta_id_padre = $this->input->post('id');
               }else{
                    $this->db->where('examen_id', $save['examen_id']);
                    $ultimo = $this->db->select('max(orden)')->get('preguntas')->row_array();
                    if ($ultimo) {
                         $save['orden'] = ++$ultimo['max(orden)'] ;
                    }else {
                         $save['orden'] = 1;
                    }

                    $pregunta_id_padre = $this->preguntas_model->insertar_pregunta($save);
               }
               $no_eliminar_preg = [];
               foreach ($preguntas as $key => $pregunta) {
                    $save_preg['titulo'] = $pregunta['titulo'];
                    $save_preg['pregunta_padre'] = $pregunta_id_padre;
                    if ($pregunta['id']) {
                         $termino = $this->preguntas_model->editar_pregunta($pregunta['id'], $save_preg);
                         $pregunta_id = $pregunta['id'];
                         array_push($no_eliminar_preg, $pregunta['id']);
                    }else{
                         $pregunta_id = $this->preguntas_model->insertar_pregunta($save_preg);
                         array_push($no_eliminar_preg, $pregunta_id);
                    }
                    $no_eliminar = [];
                    foreach ($pregunta['respuestas'] as $key => $respuestas) {
                         $respuestas['pregunta_id'] = $pregunta_id;
                         if ($respuestas['id']) {
                              $insertado = $this->preguntas_model->editar_respuesta($respuestas['id'], $respuestas);
                              array_push($no_eliminar, $respuestas['id']);
                         }else {
                              unset($respuestas['id']);
                              $insertado = $this->preguntas_model->insertar_respuesta($respuestas);
                              array_push($no_eliminar, $insertado);
                         }
                    }
                    $this->preguntas_model->eliminar_respuestas($no_eliminar, $pregunta_id);
               }

               $this->preguntas_model->eliminar_preguntas($no_eliminar_preg, $pregunta_id_padre);

               if ($this->input->post('id')) {
                    echo json_encode( array('error' => false , 'mensaje' => 'Exito al editar pregunta' ));
               }else{
                    echo json_encode( array('error' => false , 'mensaje' => 'Exito al agregar la pregunta' ));
               }
          }else{
               echo json_encode($validacion);
          }
     }

     public function validar_respuetas_correctas($array){
          if (is_array($array)) {
               foreach ($array as $key => $preguntas) {
                    if (isset($preguntas['respuestas'])) {
                         $id = in_array('1', array_column($preguntas['respuestas'], 'correcta'));
                         if ($id) {
                              return array('error' => false , 'mensaje' => 'Sin errores' );
                         }else{
                              return array('error' => true , 'mensaje' => 'Se requieren respuestas correcta para calificar' );
                         }
                    }else{
                         return array('error' => true , 'mensaje' => 'Se requieren respuestas para calificar' );
                    }
               }
          }else{
               return array('error' => true , 'mensaje' => 'Se requieren pregunas para calificar' );
          }
     }

     public function asignar_valor_array($array, $columna, $valor){
          foreach ($array as $key => $value) {
               $array[$key][$columna] = $valor;
          }
          return $array;
     }

     public function orden_preguntas_material()
     {
          $ordn = 1;
          foreach ($this->input->post('list') as $key => $pregunta) {
               $save['orden'] = $ordn;
               $termino = $this->preguntas_model->editar_pregunta($pregunta['id'], $save);
               ++$ordn;
          }
     }


}
