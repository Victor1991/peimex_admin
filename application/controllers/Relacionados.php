<?php
class Relacionados extends Admin_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model(array('relacionados_model'));
		$this->load->library('form_validation');
	}

     public function get_relacionados(){
		$curso_id = $this->input->post('cursoId');
		$array = $this->relacionados_model->get_relacionados($curso_id);
          echo json_encode($array);
     }

	public function eliminar_relacionados(){
          $relacionados_id = $this->input->post('relacionados_id');
          $mensaje  = array('mensaje' => 'Ocurrio un error', 'error' => true);
          if ($this->relacionados_model->delete_relacionados($relacionados_id)) {
               $mensaje  = array('mensaje' => 'Exito al eliminar al relacionados', 'error' => false);
          }
          echo json_encode($mensaje);
     }

	public function get_relacionados_id(){
          $relacionados_id = $this->input->post('relacionados_id');
		$data = array('curso_id' => '' ,
					'descripcion' => '' ,
					'id' => '' ,
					'porcentaje_aprobar' => '' ,
					'titulo' => ''
				);
		$data = array_merge($data, (array)$this->relacionados_model->get_relacionados_id($relacionados_id));
          echo json_encode($data);
     }

	public function data_form(){
		$this->form_validation->set_rules('nombre', 'Nombre', 'required');
          if ($this->form_validation->run()) {
               $profesor['nombre'] = $this->input->post('nombre');
               $profesor['descripcion'] = $this->input->post('descripcion');
			$profesor['curso_id'] = $this->input->post('curso_id');
               $profesor['archivo'] = $this->input->post('archivo');
			$profesor['tipo_archivo'] = $this->input->post('tipo_archivo');

               if ( $this->input->post('id')) {
                    $this->relacionados_model->editar_relacionados($this->input->post('id'), $profesor);
                    $this->session->set_flashdata('mensajes', 'Se ha actualizado el curso ');
                    $res = [
                         'status' => 'ok',
                    ];
               }else{
                    $this->relacionados_model->new_relacionados($profesor);
                    $this->session->set_flashdata('mensajes', 'Se ha creado el curso ');
                    $res = [
                         'status' => 'ok',
                    ];
               }

               echo json_encode($res);
          } else {
               $res = [
                    'status' => 'error',
                    'errores' => validation_errors()
               ];
               echo json_encode($res);
          }
	}
}
