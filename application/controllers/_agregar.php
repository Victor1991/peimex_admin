<div class="ibox-content m-b-sm border-bottom">
    <div class="row" style="margin-left: 2px;">
        <ol class="breadcrumb">
            <?= $this->breadcrumbs->show(); ?>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Nuevo Usuario</h5>

            </div>
            <div class="ibox-content">
                <?php show_alerts(); ?>
                <form method="post" action="" class="form-horizontal" role="form">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Roles</label>
                        <div class="col-sm-6">
                            <?php echo form_dropdown('rol_id', $roles_dpdwn, '', 'class="form-control" id="rol_id"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Activo</label>
                        <div class="col-sm-6">
                            <input
                                    checked
                                    type="checkbox"
                                    class="js-switch"
                                    name="activo"
                                    id="estatus"
                                    value="1"
                                    onchange="_estatus()"
                            />&nbsp;
                            <h2 style="display: inline;" id="label_activo"></h2>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nombre</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nombre">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Apellidos</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="apellidos" id="apellidos" placeholder="Apellidos">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" name="email" id="email" placeholder="email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Username</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="username" id="username" placeholder="Username">
                            <p class="help-block">Mínimo 4 carácteres (solo se permiten carácteres alfanúmericos y los signos '.', '-', '_')</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Contraseña</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" name="password" id="password" placeholder="Contraseña">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Confirmar contraseña</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" name="conf_password" id="conf_password" placeholder="Vuelva a escribir la contraseña">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <a href="<?php echo base_url('admin/usuarios'); ?>" class="btn btn-default">Cancelar</a>
                            <?php if($this->acl->permiso('edita_usuarios')): ?>
                                <button type="submit" class="btn btn-primary">Guardar</button>
                            <?php endif; ?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>