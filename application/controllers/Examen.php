<?php
class Examen extends Admin_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model(array('cursos_model', 'cursos_model', 'alumnos_model', 'examen_model'));
		$this->load->library('form_validation');
	}

     public function get_examen(){
          $curso_id = $this->input->post('curso_id');
          $tipo = $this->input->post('tipo');
		$data = array('curso_id' => '' ,
					'descripcion' => '' ,
					'id' => '' ,
					'porcentaje_aprobar' => '' ,
					'titulo' => '',
					'instrucciones' => ''
				);
		$array = array_merge($data, (array)$this->examen_model->get_examen($curso_id, $tipo));
          echo json_encode($array);
     }

	public function eliminar_examen(){
          $examen_id = $this->input->post('examen_id');
          $mensaje  = array('mensaje' => 'Ocurrio un error', 'error' => true);
          if ($this->examen_model->delete_examen($examen_id)) {
               $mensaje  = array('mensaje' => 'Exito al eliminar al examen', 'error' => false);
          }
          echo json_encode($mensaje);
     }

	public function get_examen_id(){
          $examen_id = $this->input->post('examen_id');
		$data = array('curso_id' => '' ,
					'descripcion' => '' ,
					'id' => '' ,
					'instrucciones' => '' ,
					'porcentaje_aprobar' => '' ,
					'titulo' => ''
				);
		$data = array_merge($data, (array)$this->examen_model->get_examen_id($examen_id));
          echo json_encode($data);
     }

	public function data_form(){
		$this->form_validation->set_rules('titulo', 'Titulo', 'required');



          if ($this->form_validation->run()) {
               $examen['titulo'] = $this->input->post('titulo');
               $examen['descripcion'] = $this->input->post('descripcion');
			$examen['curso_id'] = $this->input->post('curso_id');
			$examen['tipo'] = $this->input->post('tipo');
			$examen['tiempo_examen'] = $this->input->post('tiempo_examen')== 'true' ? 1 : 0;
			$examen['tiempo'] = $this->input->post('tiempo') ;
			$examen['instrucciones'] = $this->input->post('instrucciones') ;

	

               if ( $this->input->post('id')) {
                    $examen['fecha_actualizacion'] = date('Y-m-d H:m:s');
                    $this->examen_model->editar_examen($this->input->post('id'), $examen);
                    $this->session->set_flashdata('mensajes', 'Se ha actualizado el curso ');
                    $res = [
                         'status' => 'ok',
                    ];
               }else{
				$examen['fecha_cracion'] = date('Y-m-d H:m:s');
                    $this->examen_model->new_examen($examen);
                    $this->session->set_flashdata('mensajes', 'Se ha creado el curso ');
                    $res = [
                         'status' => 'ok',
                    ];
               }

               echo json_encode($res);
          } else {
               $res = [
                    'status' => 'error',
                    'errores' => validation_errors()
               ];
               echo json_encode($res);
          }
	}
}
