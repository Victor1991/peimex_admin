var elem = document.querySelector('.js-switch');
var init = new Switchery(elem);

$(document).ready(function () {
    _estatus();
});

$( "#rol_id" ).change(function() {
     _estatus();
});

function _estatus() {
    if($('#rol_id').val() == 1){
          $('#nombre_marca').hide();
          $('#marca_asignada').hide();

        // document.getElementById("lab_usr").innerHTML="Usuario / login";
    }else if($('#rol_id').val() == 3) {
         $('#marca_asignada').show();
         $('#nombre_marca').hide();
    }else{
         $('#nombre_marca').show();
         $('#marca_asignada').hide();

        // document.getElementById("lab_usr").innerHTML="Cliente / login";
    }
}
